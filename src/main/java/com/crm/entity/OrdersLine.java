package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.Data;

@ApiModel(value="com-crm-entity-OrdersLine")
@Data
@TableName(value = "orders_line")
public class OrdersLine {
    /**
     * 明细编号
     */
    @TableId(value = "odd_id", type = IdType.AUTO)
    @ApiModelProperty(value="明细编号")
    private Long oddId;

    /**
     * 订单编号
     */
    @TableField(value = "odd_order_id")
    @ApiModelProperty(value="订单编号")
    private Long oddOrderId;

    /**
     * 商品编号
     */
    @TableField(value = "odd_prod_id")
    @ApiModelProperty(value="商品编号")
    private Long oddProdId;

    /**
     * 数量
     */
    @TableField(value = "odd_count")
    @ApiModelProperty(value="数量")
    private Integer oddCount;

    /**
     * 单位
     */
    @TableField(value = "odd_unit")
    @ApiModelProperty(value="单位")
    private String oddUnit;

    /**
     * 价格
     */
    @TableField(value = "odd_price")
    @ApiModelProperty(value="价格")
    private BigDecimal oddPrice;

    @TableField(exist = false)
    private Product Product;//商品表
}