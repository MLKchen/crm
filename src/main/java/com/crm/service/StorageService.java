package com.crm.service;

import com.crm.entity.Storage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StorageService extends IService<Storage>{
    /**
     * 分页查询列表
     * @param pageIndex
     * @param prodName
     * @param stkWarehouse
     * @return
     */
    List<Storage> listPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize, @Param("prodName") String prodName, @Param("stkWarehouse") String stkWarehouse);
    /**
     * 列表数量
     * @param prodName
     * @param stkWarehouse
     * @return
     */
    int listPageCount(@Param("prodName") String prodName,@Param("stkWarehouse") String stkWarehouse);

}
