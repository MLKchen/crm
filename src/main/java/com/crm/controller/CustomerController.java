package com.crm.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.*;
import com.crm.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author CCB
 * date: 22/11/2022 下午3:59
 * Description: 客户管理模块
 */
@Controller
//@RequestMapping("/customer")
public class CustomerController {

    private Logger logger = Logger.getLogger(CustomerController.class);

    @Autowired
    private BasDictService basDictService;//数据字典
    @Autowired
    private CstCustomerService customerService;//客户信息操作
    @Autowired
    private CstLinkmanService linkmanService;//联系人操作
    @Autowired
    private CstActivityService activityService;//交往记录
    @Autowired
    private OrdersService ordersService;//历史订单
    @Autowired
    private OrdersLineService ordersLineService;//历史订单2详情
    @Autowired
    private CstLostService cstLostService;//留失管理部分


    //进入客户信息列表
    @RequestMapping(value = "/customer/list")
    public String list(Model model, @RequestParam(defaultValue = "") String custName, @RequestParam(defaultValue = "")String custNo, @RequestParam(defaultValue = "") String custRegion,
                       @RequestParam(defaultValue = "")String custManagerName, @RequestParam(defaultValue = "")String custLevelLabel, @RequestParam(defaultValue = "1")Integer index){
        List<CstCustomer> CustomerList = null;
        //获取地区集合
        List<BasDict> regionList = basDictService.getDataByType("客户地区");
        //获取客户等级集合
        List<BasDict> gradeList = basDictService.getDataByType("客户等级");

        IPage<CstCustomer> customerIPage = customerService.getAll(custName,custNo,custRegion,
                custManagerName,custLevelLabel,index);
        CustomerList = customerIPage.getRecords();
        model.addAttribute("regionList",regionList);
        model.addAttribute("gradeList",gradeList);
        model.addAttribute("CustomerList",CustomerList);
        //查询条件返回
        model.addAttribute("custName",custName);
        model.addAttribute("custNo",custNo);
        model.addAttribute("custRegion",custRegion);
        model.addAttribute("custManagerName",custManagerName);
        model.addAttribute("custLevelLabel",custLevelLabel);
        //分页数据传递
        model.addAttribute("total",customerIPage.getTotal());//总记录数
        model.addAttribute("index",customerIPage.getCurrent());//当前页数
        model.addAttribute("showSize",customerIPage.getPages());//总页数
        return "customer/list";
    }

    //进入客户信息修改页面
    @RequestMapping("/customer/edit")
    public String toEdit(Model model,String Id){
        logger.info("进入修改页面");

        CstCustomer customer = customerService.getCustomerByid(Id);
        //获取地区集合
        List<BasDict> regionList = basDictService.getDataByType("客户地区");
        //获取客户等级集合
        List<BasDict> gradeList = basDictService.getDataByType("客户等级");

        model.addAttribute("regionList",regionList);        //地区
        model.addAttribute("gradeList",gradeList);          //客户等级
        model.addAttribute("Customer",customer);

        return "customer/edit";
    }
    //修改客户基本信息
    @RequestMapping(value = "/customer/save")
    public String modifyById(CstCustomer customer){
        logger.info("修改，保存");
        if(customer.getCustId()!=null){
            //根据地区名称查询地区编号
            BasDict  basDict = basDictService.getRegionIid(customer.getCustRegion());
//            customer.setc(basDict.getDictItem());
            customerService.modifyById(customer);
        }else {
            logger.info("新增");
        }
        return "redirect:/customer/list";
    }

    //删除客户信息及关联的数据/通过客户编号删除
    @ResponseBody
    @RequestMapping(value = "/customer/del")
    public String delBycustNo(Model model,String custNo){
        //1.根据客户编号查询订单集合-获取订单详情的id
        List<Orders> ordersList = ordersService.getList(custNo);
        //2.根据ordersList集合中的id删除orders_line表中的数据
        for (Orders orders : ordersList){
            ordersLineService.del(String.valueOf(orders.getOdrId()));
        }
        //3.根据客户编号删除orders中的数据
        ordersService.del(custNo);
        //3.删除联系人
        linkmanService.delall(custNo);
        //4.删除交往记录
        activityService.delall(custNo);
        //5.删除
        customerService.delCustomer(custNo);
        model.addAttribute("delResult","true");
        return JSON.toJSONString(model);
    }



    //进入联系人列表
    @RequestMapping("/customer/linkman")
    public String linkman(Model model,String custNo,@RequestParam(defaultValue = "1")Integer index){

        System.out.println("index:"+index);

        IPage<CstLinkman> cstLinkmanIPage = linkmanService.linkList(custNo,index);
        CstCustomer cstCustomer = customerService.getCustomerBycustNo(custNo);

        model.addAttribute("linkmanList",cstLinkmanIPage.getRecords());
        model.addAttribute("cstCustomer",cstCustomer);

        //分页数据传递
        model.addAttribute("total",cstLinkmanIPage.getTotal());//总记录数
        model.addAttribute("index",cstLinkmanIPage.getCurrent());//当前页数
        model.addAttribute("showSize",cstLinkmanIPage.getPages());//总页数
        return "customer/linkman";
    }
    //进入联系人编辑页面
    @RequestMapping("/customer/linkedit")
    public String tolinkmanEdit(Model model,String lkmId){
        CstLinkman cstLinkman = linkmanService.getLinkmanById(lkmId);
        model.addAttribute("cstLinkman",cstLinkman);
        return "customer/linkedit";
    }
    //进入联系人新增页面
    @RequestMapping("/customer/linkadd")
    public String tolinkAdd(Model model,String lkmCustNo){
        model.addAttribute("lkmCustNo",lkmCustNo);
        return "customer/linkadd";
    }
    //联系人编辑保存操作
    @RequestMapping("/customer/linksave")
    public String linksave(CstLinkman cstLinkman){
        logger.info("联系人-修改/保存");
        if(cstLinkman.getLkmId()!=null){
            logger.info("联系人修改");
            linkmanService.modifyById(cstLinkman);
        }else {
            logger.info("联系人新增");
            linkmanService.addlinkman(cstLinkman);
        }
        String custNo = cstLinkman.getLkmCustNo();
        return "redirect:/customer/linkman?custNo="+custNo;
    }
    //删除联系人
    @ResponseBody
    @RequestMapping("/customer/dellinkman")
    public String dellinkman(Model model,String lkmId){
        linkmanService.dellinkman(lkmId);
        model.addAttribute("delResult","true");
        return JSON.toJSONString(model);
    }



    //进入交往记录列表
    @RequestMapping("/customer/contact")
    public String toContact(Model model,String custNo,@RequestParam(defaultValue = "1") Integer index){
        CstCustomer cstCustomer = customerService.getCustomerBycustNo(custNo);

        IPage<CstActivity> activityIPage = activityService.contactList(custNo,index);
        List<CstActivity> activity = activityIPage.getRecords();

        model.addAttribute("cstCustomer",cstCustomer);
        model.addAttribute("contactList",activity);

        //分页数据传递
        model.addAttribute("total",activityIPage.getTotal());//总记录数
        model.addAttribute("index",activityIPage.getCurrent());//当前页数
        model.addAttribute("showSize",activityIPage.getPages());//总页数
        return "customer/contact";
    }
    //进入交往记录编辑页面
    @RequestMapping("/customer/contactedit")
    public String tocontactEdit(Model model,String atvId){
        CstActivity cstActivity = activityService.getActivityById(atvId);
        cstActivity.setAtvDate(cstActivity.getAtvDate().split(" ")[0]);
        model.addAttribute("cstActivity",cstActivity);
        return "customer/contactedit";
    }
    //新建交往记录
    @RequestMapping("/customer/contactadd")
    public String contactAdd(Model model,String atvCustNo){
        model.addAttribute("atvCustNo",atvCustNo);
        return "customer/contactadd";
    }
    //交往记录编辑保存操作
    @RequestMapping("/customer/contactsave")
    public String contactsave(CstActivity cstActivity){
        logger.info("交往信息-修改/保存");
        if(cstActivity.getAtvId()!=null){
            logger.info("交往信息修改");
            activityService.modifyById(cstActivity);
        }else {
            logger.info("交往信息新增");
            activityService.addActivity(cstActivity);

        }
        String custNo = cstActivity.getAtvCustNo();
        return "redirect:/customer/contact?custNo="+custNo;
    }
    //删除交往记录
    @ResponseBody
    @RequestMapping("/customer/delActivity")
    public String delActivity(Model model,String atvId){
        activityService.delActivity(atvId);
        model.addAttribute("delResult","true");
        return JSON.toJSONString(model);
    }



    //进入历史订单
    @RequestMapping("/customer/order")
    public String toOrder(Model model,String custNo,@RequestParam(defaultValue = "1") Integer index){
        CstCustomer cstCustomer = customerService.getCustomerBycustNo(custNo);

        IPage<Orders> ordersIPage = ordersService.OrderList(custNo,index);
        List<Orders> orders = ordersIPage.getRecords();

        model.addAttribute("ordersList",orders);
        model.addAttribute("cstCustomer",cstCustomer);

        //分页数据传递
        model.addAttribute("total",ordersIPage.getTotal());//总记录数
        model.addAttribute("index",ordersIPage.getCurrent());//当前页数
        model.addAttribute("showSize",ordersIPage.getPages());//总页数
        return "customer/order";
    }
    //进入订单详情
    @RequestMapping("/customer/orderdetails")
    public String toOrderDetails(Model model,String odrId,@RequestParam(defaultValue = "1") Integer index){

        List<OrdersLine> OrdersLine = ordersLineService.ordersLineList(odrId,index);
        Orders orders = ordersService.getOne(odrId);

        String amount = ordersLineService.getSumByodrId(odrId);

        model.addAttribute("amount",amount);
        model.addAttribute("OrdersLineList",OrdersLine);
        model.addAttribute("orders",orders);

        return "customer/orderdetails";
    }


    //进入客户流失页面
    @RequestMapping("/lost/list")
    public String tolost(Model model,@RequestParam(defaultValue = "")String lstCustName,
                         @RequestParam(defaultValue = "")String lstCustManagerName,
                         @RequestParam(defaultValue = "")String lstStatus,
                         @RequestParam(defaultValue = "1")  Integer index){
        logger.info("进入客户流失页面");
        IPage<CstLost> LostIPage = cstLostService.lostList(lstCustName,lstCustManagerName,lstStatus,index);
        List<CstLost> cstLosts = LostIPage.getRecords();

        model.addAttribute("cstLostsList",cstLosts);

        //查询条件返回
        model.addAttribute("lstCustName",lstCustName);
        model.addAttribute("lstCustManagerName",lstCustManagerName);
        model.addAttribute("lstStatus",lstStatus);

        //分页数据传递
        model.addAttribute("total",LostIPage.getTotal());//总记录数
        model.addAttribute("index",LostIPage.getCurrent());//当前页数
        model.addAttribute("showSize",LostIPage.getPages());//总页数
        return "/customer/lostlist";
    }
    //进入暂缓留失页面
    @RequestMapping("/lost/temporary")
    public String totemporary(Model model,String lstId){
        CstLost cstLost = cstLostService.getLostBylstId(lstId);
        model.addAttribute("cstLost",cstLost);
        return "customer/losttemporary";
    }
    //追加暂缓措施
    @RequestMapping("/lost/temporarymodify")
    public String temporarymodify(CstLost cstLost){
        if(cstLost.getLstDelay()!=""){
            //我觉得无数据的内容不应该覆盖原有内容
            cstLostService.temporarymodify(cstLost);
        }
        return "redirect:/lost/list";
    }
    //进入留失页面
    @RequestMapping("/lost/lose")
    public String tolose(Model model,String lstId){
        CstLost cstLost = cstLostService.getLostBylstId(lstId);
        model.addAttribute("cstLost",cstLost);
        return "customer/lostlose";
    }
    //确认留失
    @RequestMapping("/lost/losemodify")
    public String losemodify(CstLost cstLost){
        cstLostService.losemodify(cstLost);
        return "redirect:/lost/list";
    }
}
