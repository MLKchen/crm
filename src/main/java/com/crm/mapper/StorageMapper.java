package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.Storage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;

@Mapper
public interface StorageMapper extends BaseMapper<Storage> {
    /**
     * 分页查询列表
     * @param pageIndex
     * @param prodName
     * @param stkWarehouse
     * @return
     */
    List<Storage> listPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize,@Param("prodName") String prodName,@Param("stkWarehouse") String stkWarehouse);
    /**
     * 列表数量
     * @param prodName
     * @param stkWarehouse
     * @return
     */
    int listPageCount(@Param("prodName") String prodName,@Param("stkWarehouse") String stkWarehouse);
}