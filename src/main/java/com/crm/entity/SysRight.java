package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-crm-entity-SysRight")
@Data
@TableName(value = "sys_right")
public class SysRight {
    @TableId(value = "right_code", type = IdType.INPUT)
    @ApiModelProperty(value="")
    private String rightCode;

    @TableField(value = "right_parent_code")
    @ApiModelProperty(value="")
    private String rightParentCode;

    @TableField(value = "right_type")
    @ApiModelProperty(value="")
    private String rightType;

    @TableField(value = "right_text")
    @ApiModelProperty(value="")
    private String rightText;

    @TableField(value = "right_url")
    @ApiModelProperty(value="")
    private String rightUrl;

    @TableField(value = "right_tip")
    @ApiModelProperty(value="")
    private String rightTip;
}