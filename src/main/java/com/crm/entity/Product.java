package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.Data;

@ApiModel(value="com-crm-entity-Product")
@Data
@TableName(value = "product")
public class Product {
    /**
     * 编号
     */
    @TableId(value = "prod_id", type = IdType.AUTO)
    @ApiModelProperty(value="编号")
    private Long prodId;

    /**
     * 商品名称
     */
    @TableField(value = "prod_name")
    @ApiModelProperty(value="商品名称")
    private String prodName;

    /**
     * 商品类型
     */
    @TableField(value = "prod_type")
    @ApiModelProperty(value="商品类型")
    private String prodType;

    /**
     * 商品介绍
     */
    @TableField(value = "prod_batch")
    @ApiModelProperty(value="商品介绍")
    private String prodBatch;

    /**
     * 单位
     */
    @TableField(value = "prod_unit")
    @ApiModelProperty(value="单位")
    private String prodUnit;

    /**
     * 价格
     */
    @TableField(value = "prod_price")
    @ApiModelProperty(value="价格")
    private BigDecimal prodPrice;

    /**
     * 描述
     */
    @TableField(value = "prod_memo")
    @ApiModelProperty(value="描述")
    private String prodMemo;
}