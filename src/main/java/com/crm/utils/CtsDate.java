package com.crm.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author CCB
 * date: 25/11/2022 下午2:39
 * Description: 用来将CTS格式的时间转换成其他格式
 */
public class CtsDate {

    public static Date toDate1(Date yyyymmdd){
        Date d = null;
        try {
            String date = yyyymmdd.toString();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            d = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

}
