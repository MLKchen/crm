package com.crm.service;

import com.crm.entity.SysRight;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRightService extends IService<SysRight>{
    /**
     * 根据角色编号查询权限集合
     * @param roleId
     * @return
     */
    List<SysRight> list(@Param("roleId") Long roleId);

}
