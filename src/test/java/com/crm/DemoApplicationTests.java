package com.crm;

import com.crm.entity.Orders;
import com.crm.service.*;
import com.crm.utils.CtsDate;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


@RunWith(SpringRunner.class)
@SpringBootTest
class DemoApplicationTests {

    @Test
    void contextLoads() {
    }

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private BasDictService basDictService;//数据字典
    @Autowired
    private CstCustomerService customerService;//客户信息操作
    @Autowired
    private CstLinkmanService linkmanService;//联系人操作
    @Autowired
    private CstActivityService activityService;//交往记录
    @Autowired
    private OrdersService ordersService;//历史订单
    @Autowired
    private OrdersLineService ordersLineService;//历史订单2详情

    @Test
    void TestRedis(){
//        stringRedisTemplate.opsForValue().set("name","czkt");

        stringRedisTemplate.opsForValue().set("qwe","132");

        System.out.println(stringRedisTemplate.opsForValue().get("qwe"));

//        Assert.assertEquals("czkt",stringRedisTemplate.opsForValue().get("name"));
//        System.out.println(stringRedisTemplate.opsForValue().get("qqq"));
    }

    @Test
    void test1(){
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        String formatDate = null;
        Date d = null;
        Date date = null;
        try {
            d = sdf.parse("Sat Dec 08 00:00:00 CST 2007");
            formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            date = ft.parse(formatDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(formatDate);
        System.out.println(date.toString());
    }

    @Test
    public void delkehu(){
        String custNo = "KH07122406540273";
        //1.根据客户编号查询订单集合-获取订单详情的id
        List<Orders> ordersList = ordersService.getList(custNo);
        //2.根据ordersList集合中的id删除orders_line表中的数据
        for (Orders orders : ordersList){
            ordersLineService.del(String.valueOf(orders.getOdrId()));
        }
        //3.根据客户编号删除orders中的数据
        ordersService.del(custNo);
        //3.删除联系人
        linkmanService.delall(custNo);
        //4.删除交往记录
        activityService.delall(custNo);
        //5.删除客户
        customerService.delCustomer(custNo);
    }

}
