package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.crm.entity.CstCustomer;

import java.util.List;

public interface CstCustomerService extends IService<CstCustomer>{

    public IPage<CstCustomer> getAll(String custName, String custNo, String custRegion,
                                     String custManagerName, String custLevelLabel, Integer index);

    public CstCustomer getCustomerByid(String id);

    public CstCustomer getCustomerBycustNo(String custNo);

    public int modifyById(CstCustomer customer);

    public int delCustomer(String custNo);

    //客户构成列表=》用于报表
    public List<CstCustomer> getCount();

}
