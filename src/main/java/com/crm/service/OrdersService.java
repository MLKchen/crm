package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrdersService extends IService<Orders>{

    public IPage<Orders> OrderList(String custNo, Integer index);

    public Orders getOne(String odrId);

    public List<Orders> getList(String custNo);

    public int del(String odrCustomer);


    //获取客户贡献值
    public List<Orders> getContribute(String custName,Integer index,Integer showsize);

    //获取客户总数
    public int getCount(String custName);

}
