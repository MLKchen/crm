package com.crm.service;

import com.crm.entity.CstService;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface CstServiceService extends IService<CstService>{

    //报表需求数量
    public List<CstService> getCountservice();

}
