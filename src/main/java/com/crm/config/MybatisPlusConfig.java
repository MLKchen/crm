package com.crm.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author CCB
 * date: 19/11/2022 下午1:53
 * Description: 描述
 */
@Configuration
public class MybatisPlusConfig {//配置分页拦截器

    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }

}
