package com.crm.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.entity.SalPlan;
import com.crm.mapper.SalPlanMapper;
import com.crm.service.SalPlanService;
@Service
public class SalPlanServiceImpl extends ServiceImpl<SalPlanMapper, SalPlan> implements SalPlanService{

}
