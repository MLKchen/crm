package com.crm.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.entity.SysUser;
import com.crm.mapper.SysUserMapper;
import com.crm.service.SysUserService;
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService{

    @Resource
    public SysUserMapper userMapper;

    @Override
    public SysUser login(String usrName, String usrPassword) {
        return userMapper.login(usrName,usrPassword);
    }
}
