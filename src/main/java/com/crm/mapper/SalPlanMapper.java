package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.SalPlan;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SalPlanMapper extends BaseMapper<SalPlan> {
}