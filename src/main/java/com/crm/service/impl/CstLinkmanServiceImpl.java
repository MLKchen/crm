package com.crm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.mapper.CstLinkmanMapper;
import com.crm.entity.CstLinkman;
import com.crm.service.CstLinkmanService;
@Service
public class CstLinkmanServiceImpl extends ServiceImpl<CstLinkmanMapper, CstLinkman> implements CstLinkmanService{

    @Resource
    private CstLinkmanMapper cstLinkmanMapper;

    @Override
    public IPage<CstLinkman> linkList(String custNo,Integer index) {
        Page<CstLinkman> page = new Page<CstLinkman>(index,5);
        QueryWrapper<CstLinkman> queryWrapper = new QueryWrapper();
        queryWrapper.eq("lkm_cust_no",custNo);
        IPage<CstLinkman> customerIPage = cstLinkmanMapper.selectPage(page,queryWrapper);
        return customerIPage;
    }

    @Override
    public CstLinkman getLinkmanById(String id) {
        QueryWrapper<CstLinkman> queryWrapper = new QueryWrapper();
        queryWrapper.eq("lkm_id",id);
        return cstLinkmanMapper.selectOne(queryWrapper);
    }

    @Override
    public int modifyById(CstLinkman cstLinkman) {
        return cstLinkmanMapper.updateById(cstLinkman);
    }

    @Override
    public int addlinkman(CstLinkman cstLinkman) {
        return cstLinkmanMapper.insert(cstLinkman);
    }

    @Override
    public int dellinkman(String lkmId) {
        return cstLinkmanMapper.deleteById(lkmId);
    }

    @Override
    public int delall(String lkmCustNo) {
        QueryWrapper<CstLinkman> queryWrapper = new QueryWrapper();
        queryWrapper.eq("lkm_cust_no",lkmCustNo);
        return cstLinkmanMapper.delete(queryWrapper);
    }
}
