package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.CstActivity;
import com.crm.entity.CstLinkman;
import com.crm.entity.Orders;
import com.crm.entity.OrdersLine;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface OrdersLineService extends IService<OrdersLine>{

    public List<OrdersLine> ordersLineList(String odrId, Integer index);

    public String getSumByodrId(String odrId);

    public int del(String oddId);

}
