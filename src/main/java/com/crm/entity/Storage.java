package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-crm-entity-Storage")
@Data
@TableName(value = "`storage`")
public class Storage {
    /**
     * 编号
     */
    @TableId(value = "stk_id", type = IdType.AUTO)
    @ApiModelProperty(value="编号")
    private Long stkId;

    /**
     * 产品编号
     */
    @TableField(value = "stk_prod_id")
    @ApiModelProperty(value="产品编号")
    private Long stkProdId;

    /**
     * 仓库
     */
    @TableField(value = "stk_warehouse")
    @ApiModelProperty(value="仓库")
    private String stkWarehouse;

    /**
     * 货位
     */
    @TableField(value = "stk_ware")
    @ApiModelProperty(value="货位")
    private String stkWare;

    /**
     * 库存数量
     */
    @TableField(value = "stk_count")
    @ApiModelProperty(value="库存数量")
    private Integer stkCount;

    /**
     * 备注
     */
    @TableField(value = "stk_memo")
    @ApiModelProperty(value="备注")
    private String stkMemo;

    @TableField(exist = false)
    private Product product;
}