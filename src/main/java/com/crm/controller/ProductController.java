package com.crm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.BasDict;
import com.crm.entity.Product;
import com.crm.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

/**
 * @author CLH
 * date: 2022/11/27 10:57
 * Description: 产品控制层
 */
@Controller
@RequestMapping("product")
public class ProductController {

    @Resource
    private ProductService productService;

    /**
     * 进入列表
     *
     * @param model
     * @param pageIndex
     * @param prodName
     * @param prodType
     * @param prodBatch
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model,
                       @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                       @RequestParam(required = false) String prodName,
                       @RequestParam(required = false) String prodType,
                       @RequestParam(required = false) String prodBatch) {
        Page page = new Page(pageIndex, 5);//页码
        /*查询条件*/
        QueryWrapper<Product> wrapper = new QueryWrapper<Product>();
        if (prodName != null && !prodName.equals("")) {
            wrapper.eq("prod_name", prodName);
            model.addAttribute("prodName", prodName);
        }
        if (prodType != null && !prodType.equals("")) {
            wrapper.eq("prod_type", prodType);
            model.addAttribute("prodType", prodType);
        }
        if (prodBatch != null && !prodBatch.equals("")) {
            wrapper.eq("prod_batch", prodBatch);
            model.addAttribute("prodBatch", prodBatch);
        }
        IPage productPage = productService.page(page, wrapper);
        System.out.println("页码：" + productPage.getCurrent());
        System.out.println("总页数：" + productPage.getPages());
        System.out.println("总行数：" + productPage.getTotal());
        System.out.println("集合：" + productPage.getRecords().toString());
        model.addAttribute("productPage", productPage);
        return "/product/list";
    }
}
