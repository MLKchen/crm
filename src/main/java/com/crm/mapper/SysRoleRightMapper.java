package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.SysRoleRight;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysRoleRightMapper extends BaseMapper<SysRoleRight> {
}