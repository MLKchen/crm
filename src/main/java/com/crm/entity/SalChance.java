package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-crm-entity-SalChance")
@Data
@TableName(value = "sal_chance")
public class SalChance {
    /**
     * 编号
     */
    @TableId(value = "chc_id", type = IdType.AUTO)
    @ApiModelProperty(value="编号")
    private Long chcId;

    /**
     * 机会来源
     */
    @TableField(value = "chc_source")
    @ApiModelProperty(value="机会来源")
    private String chcSource;

    /**
     * 客户名称
     */
    @TableField(value = "chc_cust_name")
    @ApiModelProperty(value="客户名称")
    private String chcCustName;

    /**
     * 概要
     */
    @TableField(value = "chc_title")
    @ApiModelProperty(value="概要")
    private String chcTitle;

    /**
     * 成功机率
     */
    @TableField(value = "chc_rate")
    @ApiModelProperty(value="成功机率")
    private Integer chcRate;

    /**
     * 负责人
     */
    @TableField(value = "chc_linkman")
    @ApiModelProperty(value="负责人")
    private String chcLinkman;

    /**
     * 公司电话
     */
    @TableField(value = "chc_tel")
    @ApiModelProperty(value="公司电话")
    private String chcTel;

    /**
     * 机会描述
     */
    @TableField(value = "chc_desc")
    @ApiModelProperty(value="机会描述")
    private String chcDesc;

    /**
     * 创建人编号
     */
    @TableField(value = "chc_create_id")
    @ApiModelProperty(value="创建人编号")
    private Long chcCreateId;

    /**
     * 创建人姓名
     */
    @TableField(value = "chc_create_by")
    @ApiModelProperty(value="创建人姓名")
    private String chcCreateBy;

    /**
     * 创建时间
     */
    @TableField(value = "chc_create_date")
    @ApiModelProperty(value="创建时间")
    private Date chcCreateDate;

    /**
     * 指派人编号
     */
    @TableField(value = "chc_due_id")
    @ApiModelProperty(value="指派人编号")
    private Long chcDueId;

    /**
     * 指派人姓名
     */
    @TableField(value = "chc_due_to")
    @ApiModelProperty(value="指派人姓名")
    private String chcDueTo;

    /**
     * 指派时间
     */
    @TableField(value = "chc_due_date")
    @ApiModelProperty(value="指派时间")
    private String chcDueDate;

    /**
     * 销售机会状态
     */
    @TableField(value = "chc_status")
    @ApiModelProperty(value="销售机会状态")
    private String chcStatus;
}