package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.SalChance;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SalChanceMapper extends BaseMapper<SalChance> {
}