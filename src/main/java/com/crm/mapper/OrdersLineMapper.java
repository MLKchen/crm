package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.OrdersLine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrdersLineMapper extends BaseMapper<OrdersLine> {

    //订单列表
    public List<OrdersLine> fingOrdersLineList(String oddId,Integer index);

}