package com.crm.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.entity.CstService;
import com.crm.mapper.CstServiceMapper;
import com.crm.service.CstServiceService;
@Service
public class CstServiceServiceImpl extends ServiceImpl<CstServiceMapper, CstService> implements CstServiceService{

    @Resource
    private CstServiceMapper cstServiceMapper;

    @Override
    public List<CstService> getCountservice() {
        return cstServiceMapper.getCountservice();
    }
}
