package com.crm.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.BasDict;
import com.crm.entity.CstCustomer;
import com.crm.entity.CstService;
import com.crm.service.BasDictService;
import com.crm.service.CstCustomerService;
import com.crm.service.CstServiceService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * @author CLH
 * date: 2022/11/27 10:56
 * Description: 服务类型控制层
 */
@Controller
@RequestMapping("/dict/service")
public class BasDictServiceController {
    @Resource
    private BasDictService basDictService;
    @Resource
    private CstServiceService cstServiceService;

    /**
     * 进入列表
     *
     * @param model
     * @param pageIndex
     * @param dictItem
     * @param dictValue
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model,
                       @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                       @RequestParam(required = false) Integer dictItem,
                       @RequestParam(required = false) String dictValue) {
        Page page = new Page(pageIndex, 5);//页码
        /*查询条件*/
        QueryWrapper<BasDict> wrapper = new QueryWrapper<BasDict>();
        if (dictItem != null) {
            wrapper.eq("dict_item", dictItem);
            model.addAttribute("dictItem", dictItem);
        }
        if (dictValue != null && !dictValue.equals("")) {
            wrapper.eq("dict_value", dictValue);
            model.addAttribute("dictValue", dictValue);
        }
        wrapper.eq("dict_type", "服务类型");
        Page servicePage = (Page) basDictService.page(page, wrapper);
        System.out.println("页码：" + servicePage.getCurrent());
        System.out.println("总页数：" + servicePage.getPages());
        System.out.println("总行数：" + servicePage.getTotal());
        System.out.println("集合：" + servicePage.getRecords().toString());
        model.addAttribute("servicePage", servicePage);
        return "/dict/service/list";
    }

    /**
     * 进入新增页面
     *
     * @return
     */
    @RequestMapping("/add")
    public String add() {
        return "/dict/service/add";
    }

    /**
     * 保存数据
     *
     * @return
     */
    @RequestMapping("/save")
    public String save(BasDict basDict) {
        /*条件管理器*/
        QueryWrapper<BasDict> basDictQueryWrapper = new QueryWrapper<BasDict>();//客户类型
        basDictQueryWrapper.eq("dict_type", "服务类型");
        List<BasDict> basDicts = basDictService.list(basDictQueryWrapper);
        String dictItem =  basDicts.get(basDicts.size()-1).getDictItem()+1;
        basDict.setDictItem(dictItem);
        if (basDict != null) {
            if (basDictService.save(basDict)) {
                return "redirect:/dict/service/list";
            }
        }
        return "/dict/service/add";
    }

    /**
     * 进入修改页面
     *
     * @param model
     * @param dictId
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Model model, Long dictId) {
        BasDict basDict = basDictService.getById(dictId);
        model.addAttribute("basDict", basDict);
        return "/dict/service/edit";
    }

    /**
     * 修改数据
     *
     * @param basDict
     * @return
     */
    @RequestMapping("/update")
    public String update(Model model, BasDict basDict) {
        QueryWrapper<BasDict> wrapper = new QueryWrapper<BasDict>();
        if (basDict != null) {
            wrapper.eq("dict_id", basDict.getDictId());
            wrapper.eq("dict_type", basDict.getDictType());
            if (basDictService.update(basDict, wrapper)) {
                return "redirect:/dict/service/list";
            }
        }
        model.addAttribute("basDict", basDict);
        return "/dict/service/edit";
    }

    /**
     * Ajax删除销售数据
     *
     * @param dictItem
     * @return
     */
    @ResponseBody
    @GetMapping("/del/{dictItem}")
    public Object del(@PathVariable("dictItem") Integer dictItem, HttpSession session) {
        /*条件管理器*/
        QueryWrapper<BasDict> basDictQueryWrapper = new QueryWrapper<BasDict>();//服务类型
        basDictQueryWrapper.eq("dict_item", dictItem);
        basDictQueryWrapper.eq("dict_type","服务类型");
        QueryWrapper<CstService> cstServiceQueryWrapper = new QueryWrapper<CstService>();//服务信息
        cstServiceQueryWrapper.eq("svr_type_id", dictItem);

        /*Map返回结果*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        List<CstService> cstServices = cstServiceService.list(cstServiceQueryWrapper);//根据客户地区编号查找关联的服务信息数据

        if (cstServices.size()!=0) {//判断该编号是否关联了服务表数据
            if (cstServiceService.remove(cstServiceQueryWrapper)) {
                if (basDictService.remove(basDictQueryWrapper)) {
                    hashMap.put("delResult", "reachtrue");//同时删除服务表和服务类型数据成功
                }else {
                    hashMap.put("delResult", "false");//删除服务类型数据失败
                }
            } else {
                hashMap.put("delResult", "reachfalse");//删除关联服务表数据失败
            }
        }else {
            if (basDictService.remove(basDictQueryWrapper)) {
                hashMap.put("delResult", "true");//删除服务类型数据成功
            }else {
                hashMap.put("delResult", "false");//删除服务类型数据失败
            }
        }
        return JSON.toJSONString(hashMap);
    }
}
