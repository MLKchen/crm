package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.BasDict;
import com.crm.entity.Orders;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class OrdersServiceTest {

    @Resource
    private OrdersService ordersService;

    @Resource
    private BasDictService basDictService;

    @Test
    void orderList() {
        IPage<Orders> ordersIPage = ordersService.OrderList("KH071207032218637",1);
        List<Orders> orders = ordersIPage.getRecords();
        for (Orders orders1 : orders){
            System.out.println(orders1.getOdrId());
        }
    }

    @Test
    void te(){
        BasDict basDict = basDictService.getRegionIid("河北");
        System.out.println(basDict);
    }
}