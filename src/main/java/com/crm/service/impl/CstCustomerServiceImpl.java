package com.crm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.CstCustomer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.mapper.CstCustomerMapper;
import com.crm.service.CstCustomerService;
@Service
public class CstCustomerServiceImpl extends ServiceImpl<CstCustomerMapper, CstCustomer> implements CstCustomerService{

    @Autowired
    private CstCustomerMapper customerMapper;

    @Override
    public IPage<CstCustomer> getAll(String custName, String custNo, String custRegion,
                                     String custManagerName, String custLevelLabel, Integer index) {
        Page<CstCustomer> page = new Page<CstCustomer>(index,5);
        //查询条件加入
        QueryWrapper<CstCustomer> customerCondition = new QueryWrapper<>();
        customerCondition.like("cust_name",custName);
        customerCondition.like("cust_no",custNo);
        customerCondition.like("cust_manager_name",custManagerName);
        customerCondition.like("cust_region",custRegion);
        customerCondition.like("cust_level_label",custLevelLabel);
        IPage<CstCustomer> customerIPage = customerMapper.selectPage(page,customerCondition);
        return customerIPage;
    }

    @Override
    public CstCustomer getCustomerByid(String id) {
        QueryWrapper<CstCustomer> editDisplay = new QueryWrapper<>();
        editDisplay.eq("cust_Id",id);
        return customerMapper.selectOne(editDisplay);
    }

    @Override
    public int modifyById(CstCustomer customer) {
        UpdateWrapper<CstCustomer> cstCustomerUpdateWrapper = new UpdateWrapper<>();
        cstCustomerUpdateWrapper.eq("cust_Id",customer.getCustId());
        return customerMapper.update(customer,cstCustomerUpdateWrapper);
    }

    @Override
    public CstCustomer getCustomerBycustNo(String custNo) {
        QueryWrapper<CstCustomer> editDisplay = new QueryWrapper<>();
        editDisplay.eq("cust_no",custNo);
        return customerMapper.selectOne(editDisplay);
    }

    @Override
    public int delCustomer(String custNo) {
        QueryWrapper<CstCustomer> del = new QueryWrapper<>();
        del.eq("cust_no",custNo);
        return customerMapper.delete(del);
    }

    @Override
    public List<CstCustomer> getCount() {
        return customerMapper.getCount();
    }
}
