package com.crm.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.BasDict;
import com.crm.entity.CstCustomer;
import com.crm.entity.SysRole;
import com.crm.entity.SysUser;
import com.crm.service.BasDictService;
import com.crm.service.CstCustomerService;
import com.crm.service.SysRoleService;
import com.crm.service.SysUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * @author CLH
 * date: 2022/11/28 15:02
 * Description: 用户管理控制层
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Resource
    private SysUserService sysUserService;
    @Resource
    private SysRoleService sysRoleService;

    /**
     * 进入列表
     *
     * @param model
     * @param pageIndex
     * @param usrId
     * @param usrName
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model,
                       @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                       @RequestParam(required = false) Long usrId,
                       @RequestParam(required = false) String usrName) {
        Page page = new Page(pageIndex, 5);//页码
        /*查询条件*/
        QueryWrapper<SysUser> wrapper = new QueryWrapper<SysUser>();
        if (usrId != null) {
            wrapper.eq("usr_id", usrId);
            model.addAttribute("usrId", usrId);
        }
        if (usrName != null && !usrName.equals("")) {
            wrapper.eq("usr_name", usrName);
            model.addAttribute("usrName", usrName);
        }
        Page userPage = (Page) sysUserService.page(page, wrapper);
        System.out.println("页码：" + userPage.getCurrent());
        System.out.println("总页数：" + userPage.getPages());
        System.out.println("总行数：" + userPage.getTotal());
        System.out.println("集合：" + userPage.getRecords().toString());
        model.addAttribute("userPage", userPage);
        return "/user/list";
    }

    /**
     * 进入新增页面
     *
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model) {
        List<SysRole> roles = sysRoleService.list();
        model.addAttribute("roles",roles);
        return "/user/add";
    }

    /**
     * 保存数据
     *
     * @return
     */
    @RequestMapping("/save")
    public String save(SysUser sysUser) {
        if (sysUser != null) {
            if (sysUserService.save(sysUser)) {
                return "redirect:/user/list";
            }
        }
        return "/user/add";
    }

    /**
     * 进入修改页面
     *
     * @param model
     * @param usrId
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Model model, Long usrId) {
        SysUser user = sysUserService.getById(usrId);
        model.addAttribute("user", user);
        SysRole role = sysRoleService.getById(user.getUsrRoleId());
        model.addAttribute("role",role);
        List<SysRole> roles = sysRoleService.list();
        model.addAttribute("roles",roles);
        return "/user/edit";
    }

    /**
     * 修改数据
     *
     * @param sysUser
     * @return
     */
    @RequestMapping("/update")
    public String update(Model model,SysUser sysUser) {
        QueryWrapper<SysUser> sysUserQueryWrapper = new QueryWrapper<SysUser>();
        sysUserQueryWrapper.eq("usr_id",sysUser.getUsrId());
        if (sysUser!=null){
            if (sysUserService.update(sysUser,sysUserQueryWrapper)){
                return "redirect:/user/list";
            }else {
                model.addAttribute("user", sysUser);
                SysRole role = sysRoleService.getById(sysUser.getUsrRoleId());
                model.addAttribute("role",role);
                List<SysRole> roles = sysRoleService.list();
                model.addAttribute("roles",roles);
            }
        }
        return "/user/edit";
    }

    /**
     * Ajax删除数据
     *
     * @param usrId
     * @return
     */
    @ResponseBody
    @GetMapping("/del/{usrId}")
    public Object del(@PathVariable("usrId") Long usrId) {
        /*条件管理器*/
        QueryWrapper<SysUser> sysUserQueryWrapper = new QueryWrapper<SysUser>();//客户类型
        sysUserQueryWrapper.eq("usr_id", usrId);
        /*Map返回结果*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
            if (sysUserService.remove(sysUserQueryWrapper)) {
                hashMap.put("delResult", "true");//删除成功
            }else {
                hashMap.put("delResult", "false");//删除失败
            }
        return JSON.toJSONString(hashMap);
    }
}
