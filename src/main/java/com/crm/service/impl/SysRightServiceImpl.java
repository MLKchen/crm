package com.crm.service.impl;

import com.crm.service.SysRightService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.entity.SysRight;
import com.crm.mapper.SysRightMapper;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysRightServiceImpl extends ServiceImpl<SysRightMapper, SysRight> implements SysRightService {

    @Resource
    private SysRightMapper sysRightMapper;
    @Override
    public List<SysRight> list(Long roleId) {
        return sysRightMapper.list(roleId);
    }
}
