package com.crm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.CstActivity;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.entity.CstLost;
import com.crm.mapper.CstLostMapper;
import com.crm.service.CstLostService;
@Service
public class CstLostServiceImpl extends ServiceImpl<CstLostMapper, CstLost> implements CstLostService{

    @Resource
    private CstLostMapper cstLostMapper;

    @Override
    public IPage<CstLost> lostList(String lstCustName, String lstCustManagerName,String lstStatus,  Integer index) {
        Page<CstLost> page = new Page<CstLost>(index,5);
        QueryWrapper<CstLost> queryWrapper = new QueryWrapper();
        queryWrapper.like("lst_cust_name",lstCustName);
        queryWrapper.like("lst_cust_manager_name",lstCustManagerName);
//        if(lstStatus!="" || lstStatus != null){
            queryWrapper.like("lst_status",lstStatus);
//        }
        IPage<CstLost> customerIPage = cstLostMapper.selectPage(page,queryWrapper);
        return customerIPage;
    }

    @Override
    public CstLost getLostBylstId(String lstId) {
        QueryWrapper<CstLost> queryWrapper = new QueryWrapper();
        queryWrapper.eq("lst_id",lstId);
        return cstLostMapper.selectOne(queryWrapper);
    }

    @Override
    public int temporarymodify(CstLost cstLost) {
        UpdateWrapper<CstLost> updateWrapper = new UpdateWrapper();
        updateWrapper.eq("lst_id",cstLost.getLstId());
        updateWrapper.set("lst_delay",cstLost.getLstDelay());
        updateWrapper.set("lst_status","暂缓流失");
        cstLostMapper.update(cstLost,updateWrapper);
        return 1;
    }

    @Override
    public int losemodify(CstLost cstLost) {
        UpdateWrapper<CstLost> updateWrapper = new UpdateWrapper();
        updateWrapper.eq("lst_id",cstLost.getLstId());
        updateWrapper.set("lst_reason",cstLost.getLstReason());
        updateWrapper.set("lst_status","确认留失");
        updateWrapper.set("lst_lost_date",new Date());
        cstLostMapper.update(cstLost,updateWrapper);
        return 0;
    }

    @Override
    public IPage<CstLost> getlostList(String lstCustName, String lstCustManagerName, Integer index) {
        Page<CstLost> page = new Page<CstLost>(index,5);
        QueryWrapper<CstLost> queryWrapper = new QueryWrapper();
        queryWrapper.like("lst_cust_name",lstCustName);
        queryWrapper.like("lst_cust_manager_name",lstCustManagerName);
        queryWrapper.like("lst_status","确认留失");
        IPage<CstLost> cstLostIPage = cstLostMapper.selectPage(page,queryWrapper);
        return cstLostIPage;
    }
}
