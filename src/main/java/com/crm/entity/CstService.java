package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-crm-entity-CstService")
@Data
@TableName(value = "cst_service")
public class CstService {
    /**
     * 服务编号
     */
    @TableId(value = "svr_id", type = IdType.AUTO)
    @ApiModelProperty(value="服务编号")
    private Long svrId;

    /**
     * 服务类型
     */
    @TableField(value = "svr_type")
    @ApiModelProperty(value="服务类型")
    private String svrType;

    /**
     * 概要
     */
    @TableField(value = "svr_title")
    @ApiModelProperty(value="概要")
    private String svrTitle;

    /**
     * 客户编号
     */
    @TableField(value = "svr_cust_no")
    @ApiModelProperty(value="客户编号")
    private String svrCustNo;

    /**
     * 客户姓名
     */
    @TableField(value = "svr_cust_name")
    @ApiModelProperty(value="客户姓名")
    private String svrCustName;

    /**
     * 服务状态
     */
    @TableField(value = "svr_status")
    @ApiModelProperty(value="服务状态")
    private String svrStatus;

    /**
     * 服务请求
     */
    @TableField(value = "svr_request")
    @ApiModelProperty(value="服务请求")
    private String svrRequest;

    /**
     * 创建人编号
     */
    @TableField(value = "svr_create_id")
    @ApiModelProperty(value="创建人编号")
    private Long svrCreateId;

    /**
     * 创建人姓名
     */
    @TableField(value = "svr_create_by")
    @ApiModelProperty(value="创建人姓名")
    private String svrCreateBy;

    /**
     * 创建时间
     */
    @TableField(value = "svr_create_date")
    @ApiModelProperty(value="创建时间")
    private Date svrCreateDate;

    /**
     * 分配人编号
     */
    @TableField(value = "svr_due_id")
    @ApiModelProperty(value="分配人编号")
    private Long svrDueId;

    /**
     * 分配人姓名
     */
    @TableField(value = "svr_due_to")
    @ApiModelProperty(value="分配人姓名")
    private String svrDueTo;

    /**
     * 分配时间
     */
    @TableField(value = "svr_due_date")
    @ApiModelProperty(value="分配时间")
    private Date svrDueDate;

    /**
     * 服务处理
     */
    @TableField(value = "svr_deal")
    @ApiModelProperty(value="服务处理")
    private String svrDeal;

    /**
     * 处理人编号
     */
    @TableField(value = "svr_deal_id")
    @ApiModelProperty(value="处理人编号")
    private Long svrDealId;

    /**
     * 处理人姓名
     */
    @TableField(value = "svr_deal_by")
    @ApiModelProperty(value="处理人姓名")
    private String svrDealBy;

    /**
     * 处理时间
     */
    @TableField(value = "svr_deal_date")
    @ApiModelProperty(value="处理时间")
    private String svrDealDate;

    /**
     * 处理结果
     */
    @TableField(value = "svr_result")
    @ApiModelProperty(value="处理结果")
    private String svrResult;

    /**
     * 满意度
     */
    @TableField(value = "svr_satisfy")
    @ApiModelProperty(value="满意度")
    private Integer svrSatisfy;

    /**
     * 服务类型编号
     */
    @TableField(value = "svr_type_id")
    @ApiModelProperty(value="服务类型编号")
    private Integer svrTypeId;

    /**
     * 服务数量
     */
    @TableField(exist = false)
    private Integer sumService;

    //报表
    @TableField(exist = false)
    private String value;
    //报表
    @TableField(exist = false)
    private String name;
}