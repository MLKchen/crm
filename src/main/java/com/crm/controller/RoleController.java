package com.crm.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.SysRight;
import com.crm.entity.SysRole;
import com.crm.entity.SysRoleRight;
import com.crm.entity.SysUser;
import com.crm.service.SysRightService;
import com.crm.service.SysRoleRightService;
import com.crm.service.SysRoleService;
import com.crm.service.SysUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @author CLH
 * date: 2022/11/28 16:20
 * Description: 角色管理控制层
 */
@Controller
@RequestMapping("/role")
public class RoleController {
    @Resource
    private SysRoleService sysRoleService;
    @Resource
    private SysUserService sysUserService;
    @Resource
    private SysRoleRightService sysRoleRightService;
    @Resource
    private SysRightService sysRightService;

    /**
     * 进入列表
     *
     * @param model
     * @param pageIndex
     * @param roleId
     * @param roleName
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model,
                       @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                       @RequestParam(required = false) Long roleId,
                       @RequestParam(required = false) String roleName) {
        Page page = new Page(pageIndex, 5);//页码
        /*查询条件*/
        QueryWrapper<SysRole> wrapper = new QueryWrapper<SysRole>();
        if (roleId != null) {
            wrapper.eq("role_id", roleId);
            model.addAttribute("roleId", roleId);
        }
        if (roleName != null && !roleName.equals("")) {
            wrapper.eq("role_name", roleName);
            model.addAttribute("roleName", roleName);
        }
        IPage rolePage = sysRoleService.page(page, wrapper);
        System.out.println("页码：" + rolePage.getCurrent());
        System.out.println("总页数：" + rolePage.getPages());
        System.out.println("总行数：" + rolePage.getTotal());
        System.out.println("集合：" + rolePage.getRecords().toString());
        model.addAttribute("rolePage", rolePage);
        return "/role/list";
    }

    /**
     * 进入新增页面
     *
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model) {
        List<SysRight> sysRights = sysRightService.list();
        model.addAttribute("sysRights",sysRights);
        return "/role/add";
    }

    /**
     * 保存数据
     *
     * @return
     */
    @RequestMapping("/save")
    public String save(SysRole sysRole,String[] rightCodes) {
        if (sysRoleService.save(sysRole)){
            List<SysRole> sysRoles = sysRoleService.list();
            for (String rightCode:rightCodes){
                System.out.println(rightCode);
               SysRoleRight sysRoleRight = new SysRoleRight();
               sysRoleRight.setRfRoleId(sysRoles.get(sysRoles.size()-1).getRoleId());
               sysRoleRight.setRfRightCode(rightCode);
               sysRoleRightService.save(sysRoleRight);
            }
            return "redirect:/role/list";
        }
        return "/role/add";
    }

    /**
     * 进入修改页面
     *
     * @param model
     * @param roleId
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Model model, Long roleId) {
        SysRole role = sysRoleService.getById(roleId);
        model.addAttribute("role",role);

        List<SysRight> roleRights = sysRightService.list(roleId);
        model.addAttribute("roleRights",roleRights);

        List<SysRight> sysRights = sysRightService.list();
        model.addAttribute("sysRights",sysRights);

        return "/role/edit";
    }

    /**
     * 修改数据
     *
     * @return
     */
    @RequestMapping("/update")
    public String update(SysRole sysRole,String[] rightCodes) {
        QueryWrapper<SysRoleRight> wrapper = new QueryWrapper<>();
        wrapper.eq("rf_role_id",sysRole.getRoleId());
        List<SysRoleRight> roleRights = sysRoleRightService.list(wrapper);
        if (roleRights.size() != 0) {
            if (sysRoleRightService.remove(wrapper)) {
                for (String rightCode : rightCodes) {
                    SysRoleRight sysRoleRight = new SysRoleRight();
                    sysRoleRight.setRfRoleId(sysRole.getRoleId());
                    sysRoleRight.setRfRightCode(rightCode);
                    sysRoleRightService.save(sysRoleRight);
                }
                return "redirect:/role/list";
            }
        }else {
            for (String rightCode : rightCodes) {
                SysRoleRight sysRoleRight = new SysRoleRight();
                sysRoleRight.setRfRoleId(sysRole.getRoleId());
                sysRoleRight.setRfRightCode(rightCode);
                sysRoleRightService.save(sysRoleRight);
            }
            return "redirect:/role/list";
        }
        return "/role/edit";
    }

    /**
     * Ajax删除数据
     *
     * @param roleId
     * @return
     */
    @ResponseBody
    @GetMapping("/del/{roleId}")
    public Object del(@PathVariable("roleId") Long roleId) {
        /*条件管理器*/
        QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<SysRole>();
        sysRoleQueryWrapper.eq("role_id", roleId);
        QueryWrapper<SysUser> sysUserQueryWrapper = new QueryWrapper<SysUser>();
        sysUserQueryWrapper.eq("usr_role_id",roleId);
        QueryWrapper<SysRoleRight> sysRoleRightQueryWrapper = new QueryWrapper<>();
        sysRoleRightQueryWrapper.eq("rf_role_id",roleId);
        sysRoleRightService.remove(sysRoleRightQueryWrapper);

        List<SysUser> sysUsers = sysUserService.list(sysUserQueryWrapper);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        if (sysUsers.size() !=0 ) {
            if (sysUserService.remove(sysUserQueryWrapper)) {
                if (sysRoleService.remove(sysRoleQueryWrapper)) {
                    hashMap.put("delResult", "reachtrue");//同时删除关联客户表和角色数据成功
                } else {
                    hashMap.put("delResult", "false");//删除失败
                }
            }else {
                hashMap.put("delResult", "reachfalse");//同时删除关联客户表和角色数据失败
            }
        }else {
            if (sysRoleService.remove(sysRoleQueryWrapper)) {
                hashMap.put("delResult", "true");//删除成功
            } else {
                hashMap.put("delResult", "false");//删除失败
            }
        }
        return JSON.toJSONString(hashMap);
    }
}
