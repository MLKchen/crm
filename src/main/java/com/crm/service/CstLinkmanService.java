package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.CstLinkman;
import com.baomidou.mybatisplus.extension.service.IService;

public interface CstLinkmanService extends IService<CstLinkman>{

    public IPage<CstLinkman> linkList(String custNo,Integer index);

    public CstLinkman getLinkmanById(String id);

    public int modifyById(CstLinkman cstLinkman);

    public int addlinkman(CstLinkman cstLinkman);

    public int dellinkman(String lkmId);

    public int delall(String lkmCustNo);

}
