package com.crm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.entity.BasDict;
import com.crm.mapper.BasDictMapper;
import com.crm.service.BasDictService;
@Service
public class BasDictServiceImpl extends ServiceImpl<BasDictMapper, BasDict> implements BasDictService{

    @Autowired
    private BasDictMapper basDictMapper;

    @Override
    public List<BasDict> getDataByType(String type) {
        QueryWrapper<BasDict> data = new QueryWrapper<>();
        data.eq("dict_type", type);
        return basDictMapper.selectList(data);
    }

    @Override
    public BasDict getRegionIid(String RegionName) {
        QueryWrapper<BasDict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("dict_value", RegionName);
        return basDictMapper.selectOne(queryWrapper);
    }
}
