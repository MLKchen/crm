package com.crm.config;

import com.crm.entity.SysRight;
import com.crm.entity.SysRole;
import com.crm.entity.SysUser;
import com.crm.service.SysUserService;
import org.apache.catalina.User;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * @author CCB
 * date: 12/12/2022 下午1:46
 * Description: Shiro域
 */
public class MyShiroRealm extends AuthorizingRealm {

    @Resource
    public SysUserService userService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) throws AuthenticationException {
        System.out.println("调用MySHiroRealm.doGetAuthorizationInfo获取权限信息！");

        //获得权限信息
        SysUser user = (SysUser) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        //动态授权
        SysRole role = user.getSysRole();
        if(role != null){
            info.addRole(user.getSysRole().getRoleName());
            List<SysRight> rights = role.getSysRightList();
            if(rights != null && rights.size()>0){
                for (SysRight sysRight : rights){
                    info.addStringPermission(sysRight.getRightCode());
                }
            }
        }
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("调用MySHiroRealm.doGetAuthorizationInfo获取身份信息！");

        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;

        String usrName = token.getUsername();
        String usrpwd = String.valueOf(token.getPassword());

        SysUser user = userService.login(usrName,usrpwd);

        if(user==null){
            throw new UnknownAccountException();//账号错误！
        }
        if(user.getUsrFlag()==null || user.getUsrFlag().intValue()==0){
            throw new LockedAccountException();//账号锁定
        }
        System.out.println(user.getUsrPassword());
        System.out.println(getName());

        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user,user.getUsrPassword(),getName());
        //返回身份信息
        return info;
    }
}
