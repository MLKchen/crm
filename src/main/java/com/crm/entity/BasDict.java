package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-crm-entity-BasDict")
@Data
@TableName(value = "bas_dict")
public class BasDict {
    /**
     * 编号
     */
    @TableId(value = "dict_id", type = IdType.AUTO)
    @ApiModelProperty(value="编号")
    private Long dictId;

    /**
     * 数据类型
     */
    @TableField(value = "dict_type")
    @ApiModelProperty(value="数据类型")
    private String dictType;

    /**
     * 文本
     */
    @TableField(value = "dict_item")
    @ApiModelProperty(value="文本")
    private String dictItem;

    /**
     * 值
     */
    @TableField(value = "dict_value")
    @ApiModelProperty(value="值")
    private String dictValue;

    /**
     * 是否可编辑
     */
    @TableField(value = "dict_is_editable")
    @ApiModelProperty(value="是否可编辑")
    private Byte dictIsEditable;
}