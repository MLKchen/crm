package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.CstLost;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@SpringBootTest
class CstLostServiceTest {

    @Resource
    private CstLostService cstLostService;

    @Test
    void lostList() {
        IPage<CstLost> LostIPage = cstLostService.lostList("","","",1);
        List<CstLost> cstLosts = LostIPage.getRecords();
        System.out.println(cstLosts.size());
    }

    @Test
    void te(){
        CstLost cstLost = new CstLost();
        cstLost.setLstId((long) 7);
        cstLost.setLstDelay("123465");
        cstLostService.temporarymodify(cstLost);
    }
}