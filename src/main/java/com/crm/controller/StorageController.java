package com.crm.controller;

import com.crm.entity.Storage;
import com.crm.service.StorageService;
import com.crm.utils.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CLH
 * date: 2022/11/27 10:57
 * Description: 库存控制层
 */
@Controller
@RequestMapping("/storage")
public class StorageController {

    @Resource
    private StorageService storageService;

    @RequestMapping("/list")
    public String list(Model model,
                       @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                       @RequestParam(required = false) String prodName,
                       @RequestParam(required = false) String stkWarehouse) {
        //设置页面容量
        int pageSize = 5;
        //总数量
        int totalCount = storageService.listPageCount(prodName, stkWarehouse);
        //总页数
        PageUtil pages = new PageUtil();
        pages.setNowPage(pageIndex);
        pages.setShowSize(pageSize);
        pages.setCountPage(totalCount);
        List<Storage> storageList = storageService.listPage(pages.getOffset(),pages.getShowSize(), prodName, stkWarehouse);
        model.addAttribute("storageList", storageList);
        //存储条件
        model.addAttribute("prodName", prodName);
        model.addAttribute("stkWarehouse", stkWarehouse);
        //分页存储
        model.addAttribute("pages", pages);
        return "/storage/list";
    }
}
