package com.crm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.CstLinkman;
import com.crm.mapper.CstLinkmanMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.mapper.CstActivityMapper;
import com.crm.entity.CstActivity;
import com.crm.service.CstActivityService;
@Service
public class CstActivityServiceImpl extends ServiceImpl<CstActivityMapper, CstActivity> implements CstActivityService{

    @Resource
    private CstActivityMapper cstActivityMapper;

    @Override
    public IPage<CstActivity> contactList(String custNo, Integer index) {
        Page<CstActivity> page = new Page<CstActivity>(index,5);
        QueryWrapper<CstActivity> queryWrapper = new QueryWrapper();
        queryWrapper.eq("atv_cust_no",custNo);
        IPage<CstActivity> customerIPage = cstActivityMapper.selectPage(page,queryWrapper);
        return customerIPage;
    }

    @Override
    public CstActivity getActivityById(String id) {
        QueryWrapper<CstActivity> queryWrapper = new QueryWrapper();
        queryWrapper.eq("atv_id",id);
        return cstActivityMapper.selectOne(queryWrapper);
    }

    @Override
    public int modifyById(CstActivity cstActivity) {
        return cstActivityMapper.updateById(cstActivity);
    }

    @Override
    public int addActivity(CstActivity cstActivity) {
        return cstActivityMapper.insert(cstActivity);
    }

    @Override
    public int delActivity(String atvId) {
        return cstActivityMapper.deleteById(atvId);
    }

    @Override
    public int delall(String atvCustNo) {
        QueryWrapper<CstActivity> queryWrapper = new QueryWrapper();
        queryWrapper.eq("atv_cust_no",atvCustNo);
        return cstActivityMapper.delete(queryWrapper);
    }
}
