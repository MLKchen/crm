package com.crm.service;

import com.crm.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
public interface SysUserService extends IService<SysUser>{

    //登录验证
    public SysUser login(String usrName,String usrPassword);

}
