package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-crm-entity-SalPlan")
@Data
@TableName(value = "sal_plan")
public class SalPlan {
    /**
     * 计划编号
     */
    @TableField(value = "pla_id")
    @ApiModelProperty(value="计划编号")
    private Long plaId;

    /**
     * 销售机会编号
     */
    @TableField(value = "pla_chc_id")
    @ApiModelProperty(value="销售机会编号")
    private Long plaChcId;

    /**
     * 日期
     */
    @TableField(value = "pla_date")
    @ApiModelProperty(value="日期")
    private Date plaDate;

    /**
     * 计划项
     */
    @TableField(value = "pla_todo")
    @ApiModelProperty(value="计划项")
    private String plaTodo;

    /**
     * 执行效果
     */
    @TableField(value = "pla_result")
    @ApiModelProperty(value="执行效果")
    private String plaResult;
}