package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.CstActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.crm.entity.CstLinkman;

public interface CstActivityService extends IService<CstActivity>{

    public IPage<CstActivity> contactList(String custNo, Integer index);

    public CstActivity getActivityById(String id);

    public int modifyById(CstActivity cstActivity);

    public int addActivity(CstActivity cstActivity);

    public int delActivity(String atvId);

    public int delall(String atvCustNo);
}
