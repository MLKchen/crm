package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-crm-entity-CstCustomer")
@Data
@TableName(value = "cst_customer")
public class CstCustomer {
    /**
     * 客户编号
     */
    @TableField(value = "cust_no")
    @ApiModelProperty(value="客户编号")
    private String custNo;

    /**
     * 客户名称
     */
    @TableField(value = "cust_name")
    @ApiModelProperty(value="客户名称")
    private String custName;

    /**
     * 地区
     */
    @TableField(value = "cust_region")
    @ApiModelProperty(value="地区")
    private String custRegion;

    /**
     * 负责人编号
     */
    @TableField(value = "cust_manager_id")
    @ApiModelProperty(value="负责人编号")
    private Long custManagerId;

    /**
     * 负责人姓名
     */
    @TableField(value = "cust_manager_name")
    @ApiModelProperty(value="负责人姓名")
    private String custManagerName;

    /**
     * 客户等级
     */
    @TableField(value = "cust_level")
    @ApiModelProperty(value="客户等级")
    private Integer custLevel;

    /**
     * 客户等级名称
     */
    @TableField(value = "cust_level_label")
    @ApiModelProperty(value="客户等级名称")
    private String custLevelLabel;

    /**
     * 满意度
     */
    @TableField(value = "cust_satisfy")
    @ApiModelProperty(value="满意度")
    private Integer custSatisfy;

    /**
     * 信誉度
     */
    @TableField(value = "cust_credit")
    @ApiModelProperty(value="信誉度")
    private Integer custCredit;

    /**
     * 地址
     */
    @TableField(value = "cust_addr")
    @ApiModelProperty(value="地址")
    private String custAddr;

    /**
     * 邮政编码
     */
    @TableField(value = "cust_zip")
    @ApiModelProperty(value="邮政编码")
    private String custZip;

    /**
     * 电话
     */
    @TableField(value = "cust_tel")
    @ApiModelProperty(value="电话")
    private String custTel;

    /**
     * 传真
     */
    @TableField(value = "cust_fax")
    @ApiModelProperty(value="传真")
    private String custFax;

    /**
     * 网址
     */
    @TableField(value = "cust_website")
    @ApiModelProperty(value="网址")
    private String custWebsite;

    /**
     * 营业执照注册号
     */
    @TableField(value = "cust_licence_no")
    @ApiModelProperty(value="营业执照注册号")
    private String custLicenceNo;

    /**
     * 法人
     */
    @TableField(value = "cust_chieftain")
    @ApiModelProperty(value="法人")
    private String custChieftain;

    /**
     * 注册资金
     */
    @TableField(value = "cust_bankroll")
    @ApiModelProperty(value="注册资金")
    private Long custBankroll;

    /**
     * 年营业额
     */
    @TableField(value = "cust_turnover")
    @ApiModelProperty(value="年营业额")
    private Long custTurnover;

    /**
     * 卡户银行
     */
    @TableField(value = "cust_bank")
    @ApiModelProperty(value="卡户银行")
    private String custBank;

    /**
     * 银行账号
     */
    @TableField(value = "cust_bank_account")
    @ApiModelProperty(value="银行账号")
    private String custBankAccount;

    /**
     * 地税登记号
     */
    @TableField(value = "cust_local_tax_no")
    @ApiModelProperty(value="地税登记号")
    private String custLocalTaxNo;

    /**
     * 国税登记号
     */
    @TableField(value = "cust_national_tax_no")
    @ApiModelProperty(value="国税登记号")
    private String custNationalTaxNo;

    /**
     * 客户状态
     */
    @TableField(value = "cust_status")
    @ApiModelProperty(value="客户状态")
    private String custStatus;

    /**
     * 主键
     */
    @TableField(value = "cust_Id")
    @ApiModelProperty(value="主键")
    private String custId;

    /**
     * 地区编号
     */
    @TableField(value = "cust_region_id")
    @ApiModelProperty(value="地区编号")
    private Integer custRegionId;

    /**
     * 客户等级数量
     */
    @TableField(exist = false)
    private int SumcustLevelLabel;

    @TableField(exist = false)
    private String value;

    @TableField(exist = false)
    private String name;
}