package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-crm-entity-CstLost")
@Data
@TableName(value = "cst_lost")
public class CstLost {
    /**
     * 编号
     */
    @TableField(value = "lst_id")
    @ApiModelProperty(value="编号")
    private Long lstId;

    /**
     * 客户编号
     */
    @TableField(value = "lst_cust_no")
    @ApiModelProperty(value="客户编号")
    private String lstCustNo;

    /**
     * 客户姓名
     */
    @TableField(value = "lst_cust_name")
    @ApiModelProperty(value="客户姓名")
    private String lstCustName;

    /**
     * 客户经理编号
     */
    @TableField(value = "lst_cust_manager_id")
    @ApiModelProperty(value="客户经理编号")
    private Long lstCustManagerId;

    /**
     * 客户经理姓名
     */
    @TableField(value = "lst_cust_manager_name")
    @ApiModelProperty(value="客户经理姓名")
    private String lstCustManagerName;

    /**
     * 上次下单时间
     */
    @TableField(value = "lst_last_order_date")
    @ApiModelProperty(value="上次下单时间")
    private Date lstLastOrderDate;

    /**
     * 确认流失时间
     */
    @TableField(value = "lst_lost_date")
    @ApiModelProperty(value="确认流失时间")
    private Date lstLostDate;

    /**
     * 暂缓措施
     */
    @TableField(value = "lst_delay")
    @ApiModelProperty(value="暂缓措施")
    private String lstDelay;

    /**
     * 流失原因
     */
    @TableField(value = "lst_reason")
    @ApiModelProperty(value="流失原因")
    private String lstReason;

    /**
     * 状态
     */
    @TableField(value = "lst_status")
    @ApiModelProperty(value="状态")
    private String lstStatus;
}