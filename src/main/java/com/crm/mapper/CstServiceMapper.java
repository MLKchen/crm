package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.CstService;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CstServiceMapper extends BaseMapper<CstService> {

    //报表需求数量
    public List<CstService> getCountservice();

}