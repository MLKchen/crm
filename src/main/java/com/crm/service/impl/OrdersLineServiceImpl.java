package com.crm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.CstActivity;
import com.crm.mapper.CstActivityMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.entity.OrdersLine;
import com.crm.mapper.OrdersLineMapper;
import com.crm.service.OrdersLineService;
@Service
public class OrdersLineServiceImpl extends ServiceImpl<OrdersLineMapper, OrdersLine> implements OrdersLineService{

    @Resource
    private OrdersLineMapper ordersLineMapper;

    @Override
    public List<OrdersLine> ordersLineList(String odrId, Integer index) {
        List<OrdersLine> ordersLineList = ordersLineMapper.fingOrdersLineList(odrId,index);
        return ordersLineList;
    }

    @Override
    public String getSumByodrId(String odrId) {
        QueryWrapper<OrdersLine> queryWrapper = new QueryWrapper();
        queryWrapper.eq("odd_order_id", odrId );
        queryWrapper.select("IFNULL(sum(odd_price),0) AS num");
        Map<String, Object> map = this.getMap(queryWrapper);
        return map.get("num").toString();
    }

    @Override
    public int del(String oddId) {
        QueryWrapper<OrdersLine> queryWrapper = new QueryWrapper();
        queryWrapper.eq("odd_order_id", oddId );
        return ordersLineMapper.delete(queryWrapper);

    }
}
