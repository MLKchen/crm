package com.crm.service.impl;

import com.crm.entity.SysRight;
import com.crm.entity.SysUser;
import com.crm.service.SysUserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class SysUserServiceImplTest {

    @Resource
    public SysUserService userService;

    @Test
    void login() {
        SysUser sysUser = userService.login("test1","123456");
        System.out.println(sysUser.getSysRole().getSysRightList());
        List<SysRight> userList = sysUser.getSysRole().getSysRightList();
        for (SysRight user:userList){
            System.out.println(user.getRightUrl());
        }
    }
}