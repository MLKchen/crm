package com.crm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.crm.entity.CstLinkman;
import com.crm.mapper.CstLinkmanMapper;
import com.crm.service.CstLinkmanService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class CstLinkmanServiceImplTest {

    @Resource
    private CstLinkmanMapper cstLinkmanMapper;

    @Resource
    private CstLinkmanService linkmanService;

    @Test
    void linkList() {
        QueryWrapper<CstLinkman> queryWrapper = new QueryWrapper();
        queryWrapper.eq("lkm_cust_no","KH071207032218637");
        List<CstLinkman> cstLinkmanList = cstLinkmanMapper.selectList(queryWrapper);
        for (CstLinkman cstLinkman:cstLinkmanList){
            System.out.println(cstLinkman.getLkmName());
        }
    }

    @Test
    void getOne(){
        CstLinkman cstLinkman = linkmanService.getLinkmanById("4");
        System.out.println(cstLinkman.getLkmName());


    }
}