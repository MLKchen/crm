package com.crm.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.mapper.ProductMapper;
import com.crm.entity.Product;
import com.crm.service.ProductService;
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService{

}
