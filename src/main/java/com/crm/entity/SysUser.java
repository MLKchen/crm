package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel(value="com-crm-entity-SysUser")
@Data
@TableName(value = "sys_user")
public class SysUser {
    /**
     * 编号
     */
    @TableId(value = "usr_id", type = IdType.AUTO)
    @ApiModelProperty(value="编号")
    private Long usrId;

    /**
     * 姓名
     */
    @TableField(value = "usr_name")
    @ApiModelProperty(value="姓名")
    private String usrName;

    /**
     * 密码
     */
    @TableField(value = "usr_password")
    @ApiModelProperty(value="密码")
    private String usrPassword;

    /**
     * 角色编号
     */
    @TableField(value = "usr_role_id")
    @ApiModelProperty(value="角色编号")
    private Long usrRoleId;

    /**
     * 状态
     */
    @TableField(value = "usr_flag")
    @ApiModelProperty(value="状态")
    private Integer usrFlag;

    @TableField(exist = false)
    private SysRole sysRole;
}