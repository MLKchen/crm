package com.crm.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.mapper.SalChanceMapper;
import com.crm.entity.SalChance;
import com.crm.service.SalChanceService;
@Service
public class SalChanceServiceImpl extends ServiceImpl<SalChanceMapper, SalChance> implements SalChanceService{

}
