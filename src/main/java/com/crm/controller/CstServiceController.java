package com.crm.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.BasDict;
import com.crm.entity.CstCustomer;
import com.crm.entity.CstService;
import com.crm.entity.SysUser;
import com.crm.mapper.CstServiceMapper;
import com.crm.service.BasDictService;
import com.crm.service.CstCustomerService;
import com.crm.service.CstServiceService;
import com.crm.service.SysUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author CCB
 * date: 28/11/2022 下午2:36
 * Description: 服务管理模块
 */
@Controller
@RequestMapping("/service")
public class CstServiceController {

    @Resource
    private CstCustomerService customerService;
    @Resource
    private BasDictService basDictService;
    @Resource
    private CstServiceService cstServiceService;
    @Resource
    private SysUserService sysUserService;

    //跳转至服务创建
    @RequestMapping("/add")
    public String toServiceAdd(Model model){
        //1.获取客户列表
        List<CstCustomer> customerList = customerService.list();
        //2.获取服务类型列表
        List<BasDict> basDictList= basDictService.getDataByType("服务类型");
        model.addAttribute("customer",customerList);
        model.addAttribute("basDict",basDictList);
        return "/service/add";
    }
    //服务创建
    @RequestMapping("/serviceAdd")
    public String ServiceAdd(CstService cstService){
        //根据服务类型id查询类型名称
        QueryWrapper<BasDict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("dict_type","服务类型");
        queryWrapper.eq("dict_item",cstService.getSvrTypeId());
        BasDict basDict = basDictService.getOne(queryWrapper);
        cstService.setSvrType(basDict.getDictValue());
        cstServiceService.save(cstService);
        return "redirect:/service/add";
    }

    //跳转至服务分配页面
    @RequestMapping("/dispatch")
    public String toDistribution(Model model, @RequestParam(defaultValue = "")String svrCustName,
                                 @RequestParam(defaultValue = "")String svrTitle, String svrType,
                                 @RequestParam(defaultValue = "1")Integer pageIndex ){
        Page page = new Page(pageIndex, 5);//页码
        //1.获取服务类型列表
        List<BasDict> basDictList= basDictService.getDataByType("服务类型");

        //2.获取所有客户经理
        QueryWrapper<SysUser> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("usr_role_id","2");
        List<SysUser> sysUserList = sysUserService.list(userQueryWrapper);

        //3.获取全部状态为新创建的服务
        QueryWrapper<CstService> servicequeryWrapper = new QueryWrapper<>();
        servicequeryWrapper.eq("svr_status","新创建");
        servicequeryWrapper.like("svr_cust_name",svrCustName);
        servicequeryWrapper.like("svr_title",svrTitle);
        if(svrType!=null && svrType !=""){
            servicequeryWrapper.like("svr_type_id",svrType);
        }
        IPage regionPage = cstServiceService.page(page, servicequeryWrapper);
        List<CstService> serviceList = regionPage.getRecords();

        model.addAttribute("basDict",basDictList);
        model.addAttribute("sysUser",sysUserList);
        model.addAttribute("service",serviceList);
        //分页数据传递
        model.addAttribute("total",regionPage.getTotal());//总记录数
        model.addAttribute("index",regionPage.getCurrent());//当前页数
        model.addAttribute("showSize",regionPage.getPages());//总页数

        //查询条件返回
        model.addAttribute("svrCustName",svrCustName);
        model.addAttribute("svrTitle",svrTitle);
        model.addAttribute("svrType",svrType);
        System.out.println(1);
        return "/service/distribution";
    }

    //执行分配任务
    @RequestMapping("/distribute")
    public String distribute(String svrDueId,String svrId){
        //1.根据分配编号查询分配人姓名
        QueryWrapper<SysUser> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("usr_id",svrDueId);
        String UsrName = sysUserService.getOne(userQueryWrapper).getUsrName();

        UpdateWrapper<CstService> serviceUpdateWrapper = new UpdateWrapper<>();
        serviceUpdateWrapper.set("svr_status","已分配");
        serviceUpdateWrapper.set("svr_due_id",svrDueId);
        serviceUpdateWrapper.set("svr_due_to",UsrName);
        serviceUpdateWrapper.set("svr_due_date",new Date());
        serviceUpdateWrapper.eq("svr_id",svrId);
        cstServiceService.update(serviceUpdateWrapper);
        return "redirect:/service/dispatch";
    }


    //删除服务
    @ResponseBody
    @RequestMapping("/delService")
    public String delService(Model model,String svrId){
        QueryWrapper<CstService> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("svr_id",svrId);
        cstServiceService.remove(userQueryWrapper);
        model.addAttribute("delResult","true");
        return JSON.toJSONString(model);
    }

    //进入服务处理页面
    @RequestMapping("/deal")
    public String toHandle(Model model, @RequestParam(defaultValue = "")String svrCustName,
                           @RequestParam(defaultValue = "")String svrTitle, String svrType,
                           @RequestParam(defaultValue = "1")Integer pageIndex){
        Page page = new Page(pageIndex, 5);//页码

        //1.获取服务类型列表
        List<BasDict> basDictList= basDictService.getDataByType("服务类型");

        QueryWrapper<CstService> servicequeryWrapper = new QueryWrapper<>();
        servicequeryWrapper.eq("svr_status","已分配");
        servicequeryWrapper.like("svr_cust_name",svrCustName);
        servicequeryWrapper.like("svr_title",svrTitle);

        if(svrType!=null && svrType !=""){
            servicequeryWrapper.like("svr_type_id",svrType);
        }
        IPage regionPage = cstServiceService.page(page, servicequeryWrapper);
        List<CstService> serviceList = regionPage.getRecords();

        System.out.println("serviceList:"+serviceList.size());

        model.addAttribute("basDict",basDictList);
        model.addAttribute("service",serviceList);
        //分页数据传递
        model.addAttribute("total",regionPage.getTotal());//总记录数
        model.addAttribute("index",regionPage.getCurrent());//当前页数
        model.addAttribute("showSize",regionPage.getPages());//总页数

        //查询条件返回
        model.addAttribute("svrCustName",svrCustName);
        model.addAttribute("svrTitle",svrTitle);
        model.addAttribute("svrType",svrType);
        return "/service/handle";
    }

    //处理操作页面
    @RequestMapping("/servicehandle")
    public String handle(Model model,String svrId){
        QueryWrapper<CstService> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("svr_id",svrId);
        CstService cstService = cstServiceService.getOne(queryWrapper);
        model.addAttribute("cstService",cstService);
        return "/service/servicehandle";
    }

    //执行处理
    @RequestMapping("/executeHandle")
    public String executeHandle(CstService cstService){
        UpdateWrapper<CstService> cstServiceUpdateWrapper = new UpdateWrapper<>();
        cstServiceUpdateWrapper.set("svr_deal",cstService.getSvrDeal());
        cstServiceUpdateWrapper.set("svr_deal_id",cstService.getSvrDealId());
        cstServiceUpdateWrapper.set("svr_deal_by",cstService.getSvrDealBy());
        cstServiceUpdateWrapper.set("svr_deal_date",cstService.getSvrDealDate());
        cstServiceUpdateWrapper.set("svr_deal",cstService.getSvrDeal());
        cstServiceUpdateWrapper.eq("svr_id",cstService.getSvrId());
        cstServiceUpdateWrapper.set("svr_status","已处理");
        cstServiceService.update(cstServiceUpdateWrapper);
        return "redirect:/service/deal";
    }

    //进入反馈页面
    @RequestMapping("/feedback")
    public String tofeedback(Model model, @RequestParam(defaultValue = "")String svrCustName,
                             @RequestParam(defaultValue = "")String svrTitle, String svrType,
                             @RequestParam(defaultValue = "1")Integer pageIndex){
        Page page = new Page(pageIndex, 5);//页码

        //1.获取服务类型列表
        List<BasDict> basDictList= basDictService.getDataByType("服务类型");

        QueryWrapper<CstService> servicequeryWrapper = new QueryWrapper<>();
        servicequeryWrapper.eq("svr_status","已处理");
        servicequeryWrapper.like("svr_cust_name",svrCustName);
        servicequeryWrapper.like("svr_title",svrTitle);

        if(svrType!=null && svrType !=""){
            servicequeryWrapper.like("svr_type_id",svrType);
        }
        IPage regionPage = cstServiceService.page(page, servicequeryWrapper);
        List<CstService> serviceList = regionPage.getRecords();

        model.addAttribute("basDict",basDictList);
        model.addAttribute("service",serviceList);
        //分页数据传递
        model.addAttribute("total",regionPage.getTotal());//总记录数
        model.addAttribute("index",regionPage.getCurrent());//当前页数
        model.addAttribute("showSize",regionPage.getPages());//总页数

        //查询条件返回
        model.addAttribute("svrCustName",svrCustName);
        model.addAttribute("svrTitle",svrTitle);
        model.addAttribute("svrType",svrType);
        return "/service/feedback";
    }

    //进入反馈处理页面
    @RequestMapping("/toservicefeedback")
    public String toservicefeedback(Model model,String svrId){
        QueryWrapper<CstService> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("svr_id",svrId);
        CstService cstService = cstServiceService.getOne(queryWrapper);
        model.addAttribute("cstService",cstService);
        return "/service/servicefeedback";
    }

    //执行处理完成后续操作
    @RequestMapping("/servicefeedback")
    public String servicefeedback(Model model,CstService cstService){
        UpdateWrapper<CstService> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("svr_id",cstService.getSvrId());
        updateWrapper.set("svr_result",cstService.getSvrResult());
        updateWrapper.set("svr_satisfy",cstService.getSvrSatisfy());
        updateWrapper.set("svr_status","已归档");
        cstServiceService.update(updateWrapper);
        return "redirect:/service/feedback";
    }


    //静入归档页面
    @RequestMapping("/arch")
    public String tofile(Model model, @RequestParam(defaultValue = "")String svrCustName,
                         @RequestParam(defaultValue = "")String svrTitle, String svrType,
                         @RequestParam(defaultValue = "1")Integer pageIndex){
        Page page = new Page(pageIndex, 5);//页码

        //1.获取服务类型列表
        List<BasDict> basDictList= basDictService.getDataByType("服务类型");

        QueryWrapper<CstService> servicequeryWrapper = new QueryWrapper<>();
        servicequeryWrapper.eq("svr_status","已归档");
        servicequeryWrapper.like("svr_cust_name",svrCustName);
        servicequeryWrapper.like("svr_title",svrTitle);

        if(svrType!=null && svrType !=""){
            servicequeryWrapper.like("svr_type_id",svrType);
        }
        IPage regionPage = cstServiceService.page(page, servicequeryWrapper);
        List<CstService> serviceList = regionPage.getRecords();

        model.addAttribute("basDict",basDictList);
        model.addAttribute("service",serviceList);
        //分页数据传递
        model.addAttribute("total",regionPage.getTotal());//总记录数
        model.addAttribute("index",regionPage.getCurrent());//当前页数
        model.addAttribute("showSize",regionPage.getPages());//总页数

        //查询条件返回
        model.addAttribute("svrCustName",svrCustName);
        model.addAttribute("svrTitle",svrTitle);
        model.addAttribute("svrType",svrType);
        return "/service/file";
    }

    //进入归档详情页面
    @RequestMapping("/file")
    public String file(Model model,String svrId){
        QueryWrapper<CstService> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("svr_id",svrId);
        CstService cstService = cstServiceService.getOne(queryWrapper);
        model.addAttribute("cstService",cstService);
        return "/service/servicefile";
    }


}
