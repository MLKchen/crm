package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.CstLost;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CstLostMapper extends BaseMapper<CstLost> {
}