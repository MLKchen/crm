package com.crm.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.SalChance;
import com.crm.entity.SalPlan;
import com.crm.entity.SysUser;
import com.crm.service.SalChanceService;
import com.crm.service.SalPlanService;
import com.crm.service.SysUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @author CLH
 * date: 2022/11/24 16:04
 * Description: 开发计划控制层
 */
@Controller
@RequestMapping("/plan")
public class SalPlanController {
    @Resource
    private SysUserService sysUserService;
    @Resource
    private SalChanceService salChanceService;
    @Resource
    private SalPlanService salPlanService;

    /**
     * 进入资源列表页面
     *
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model,
                       @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                       @RequestParam(required = false) String custName,
                       @RequestParam(required = false) String chcLinkman,
                       @RequestParam(required = false) String chcDesc) {
        Page page = new Page(pageIndex, 5);//页码
        /*查询条件*/
        QueryWrapper<SalChance> wrapper = new QueryWrapper<SalChance>();
        if (custName != null && !custName.equals("")) {//判断名称不为空
            wrapper.eq("chc_cust_name", custName);
            model.addAttribute("custName", custName);
        }
        if (chcLinkman != null && !chcLinkman.equals("")) {//判断责任人不为空
            wrapper.eq("chc_linkman", chcLinkman);
            model.addAttribute("chc_linkman", chcLinkman);
        }
        if (chcDesc != null && !chcDesc.equals("")) {//判断概要不为空
            wrapper.eq("chc_desc", chcDesc);
            model.addAttribute("chcDesc", chcDesc);
        }
        IPage chancePage = salChanceService.page(page, wrapper);
        System.out.println("页码：" + chancePage.getCurrent());
        System.out.println("总页数：" + chancePage.getPages());
        System.out.println("总行数：" + chancePage.getTotal());
        System.out.println("集合：" + chancePage.getRecords().toString());
        model.addAttribute("chancePage", chancePage);
        return "/plan/list";
    }
    /**
     * 进入制定开发计划页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model,Long chcId,@RequestParam(required = false, defaultValue = "1") Integer pageIndex) {
        SalChance chance = salChanceService.getById(chcId);
        if (chance != null) {
            model.addAttribute("chance", chance);
        }
        Page page = new Page(pageIndex, 3);//页码
        QueryWrapper<SalPlan> salPlanQueryWrapper = new QueryWrapper<SalPlan>();
        salPlanQueryWrapper.eq("pla_chc_id", chcId);
        IPage planPage = salPlanService.page(page, salPlanQueryWrapper);
        model.addAttribute("planPage",planPage);
        return "/plan/add";
    }

    /**
     * 进入执行开发计划页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Model model,Long chcId,@RequestParam(required = false, defaultValue = "1") Integer pageIndex) {
        SalChance chance = salChanceService.getById(chcId);
        if (chance != null) {
            model.addAttribute("chance", chance);
        }
        Page page = new Page(pageIndex, 3);//页码
        QueryWrapper<SalPlan> salPlanQueryWrapper = new QueryWrapper<SalPlan>();
        salPlanQueryWrapper.eq("pla_chc_id", chcId);
        IPage planPage = salPlanService.page(page, salPlanQueryWrapper);
        model.addAttribute("planPage",planPage);
        return "/plan/edit";
    }

    /**
     * 进入查看开发计划页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/view")
    public String view(Model model,Long chcId,@RequestParam(required = false, defaultValue = "1") Integer pageIndex) {
        SalChance chance = salChanceService.getById(chcId);
        if (chance != null) {
            model.addAttribute("chance", chance);
        }
        Page page = new Page(pageIndex, 3);//页码
        QueryWrapper<SalPlan> salPlanQueryWrapper = new QueryWrapper<SalPlan>();
        salPlanQueryWrapper.eq("pla_chc_id", chcId);
        IPage planPage = salPlanService.page(page, salPlanQueryWrapper);
        model.addAttribute("planPage",planPage);
        return "/plan/view";
    }

    /**
     * 新增开发计划
     * @param chcId
     * @param plaTodo
     * @return
     */
    @ResponseBody
    @GetMapping("/save")
    public Object save(Long chcId,String plaTodo) {
        /*条件管理器*/
        QueryWrapper<SalChance> salChanceQueryWrapper = new QueryWrapper<SalChance>();
        salChanceQueryWrapper.eq("chc_id", chcId);

        SalPlan salPlan = new SalPlan();
        salPlan.setPlaChcId(chcId);
        salPlan.setPlaTodo(plaTodo);
        /*Map返回结果*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        List<SalChance> salChances = salChanceService.list(salChanceQueryWrapper);
        if (salChances.size() == 0) {
            hashMap.put("addResult", "false");//该销售记录不存在
        } else {
            if (salPlanService.save(salPlan)) {
                hashMap.put("addResult", "true");//开发成功
            } else {
                hashMap.put("addResult", "notexist");//开发失败
            }
        }
        return JSON.toJSONString(hashMap);
    }

    /**
     * Ajax删除销售数据
     *
     * @param plaId
     * @return
     */
    @ResponseBody
    @GetMapping("/del/{plaId}")
    public Object del(@PathVariable("plaId") Long plaId) {
        /*条件管理器*/
        QueryWrapper<SalPlan> salPlanQueryWrapper = new QueryWrapper<SalPlan>();
        salPlanQueryWrapper.eq("pla_id", plaId);
        /*Map返回结果*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        if (salPlanService.remove(salPlanQueryWrapper)) {
            hashMap.put("delResult", "true");//删除成功
        } else {
            hashMap.put("delResult", "false");//删除失败
        }
        return JSON.toJSONString(hashMap);
    }

    /**
     * 保存执行效果
     * @param plaId
     * @param plaResult
     * @return
     */
    @ResponseBody
    @GetMapping("/effect")
    public Object effect(Long plaId,String plaResult) {
        QueryWrapper<SalPlan> wrapper = new QueryWrapper<>();
        wrapper.eq("pla_id",plaId);
        SalPlan salPlan = new SalPlan();
        salPlan.setPlaResult(plaResult);
        /*Map返回结果*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
            if (salPlanService.update(salPlan,wrapper)) {
                hashMap.put("updResult", "true");//保存执行效果成功
            } else {
                hashMap.put("updResult", "notexist");//开保存执行效果失败
            }
        return JSON.toJSONString(hashMap);
    }


    /**
     * Ajax开发计划成功
     *
     * @param chcId
     * @return
     */
    @ResponseBody
    @GetMapping("/development/{chcId}")
    public Object development(@PathVariable("chcId") Long chcId) {
        /*条件管理器*/
        QueryWrapper<SalChance> salChanceQueryWrapper = new QueryWrapper<SalChance>();
        salChanceQueryWrapper.eq("chc_id", chcId);
        SalChance salChance = new SalChance();
        salChance.setChcStatus("3");
        QueryWrapper<SalPlan> salPlanQueryWrapper = new QueryWrapper<SalPlan>();
        salPlanQueryWrapper.eq("pla_chc_id", chcId);

        /*Map返回结果*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        List<SalPlan> salPlans = salPlanService.list(salPlanQueryWrapper);
        if (salPlans.size() == 0) {
            hashMap.put("updResult", "false");//该销售记录还没有制定开发计划
        } else {
            if (salChanceService.update(salChance,salChanceQueryWrapper)) {
                hashMap.put("updResult", "true");//修改成功
            } else {
                hashMap.put("updResult", "notexist");//修改失败
            }
        }
        return JSON.toJSONString(hashMap);
    }

    /**
     * Ajax终止开发计划
     *
     * @param chcId
     * @return
     */
    @ResponseBody
    @GetMapping("/termination/{chcId}")
    public Object termination(@PathVariable("chcId") Long chcId) {
        /*条件管理器*/
        QueryWrapper<SalChance> salChanceQueryWrapper = new QueryWrapper<SalChance>();
        salChanceQueryWrapper.eq("chc_id", chcId);
        SalChance salChance = new SalChance();
        salChance.setChcStatus("4");
        /*Map返回结果*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
            if (salChanceService.update(salChance,salChanceQueryWrapper)) {
                hashMap.put("updResult", "true");//修改成功
            } else {
                hashMap.put("updResult", "notexist");//修改失败
            }
        return JSON.toJSONString(hashMap);
    }
}
