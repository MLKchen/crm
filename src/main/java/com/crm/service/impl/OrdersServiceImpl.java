package com.crm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.CstActivity;
import com.crm.mapper.CstActivityMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.mapper.OrdersMapper;
import com.crm.entity.Orders;
import com.crm.service.OrdersService;
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService{

    @Resource
    private OrdersMapper ordersMapper;

    @Override
    public IPage<Orders> OrderList(String custNo, Integer index) {
        Page<Orders> page = new Page<Orders>(index,5);
        QueryWrapper<Orders> queryWrapper = new QueryWrapper();
        queryWrapper.eq("odr_customer",custNo);
        IPage<Orders> OrdersIPage = ordersMapper.selectPage(page,queryWrapper);
        return OrdersIPage;
    }

    @Override
    public Orders getOne(String odrId) {
        QueryWrapper<Orders> queryWrapper = new QueryWrapper();
        queryWrapper.eq("odr_id",odrId);
        return ordersMapper.selectOne(queryWrapper);
    }

    @Override
    public List<Orders> getList(String custNo) {
        QueryWrapper<Orders> queryWrapper = new QueryWrapper();
        queryWrapper.eq("odr_customer",custNo);
        return ordersMapper.selectList(queryWrapper);
    }

    @Override
    public int del(String odrCustomer) {
        QueryWrapper<Orders> queryWrapper = new QueryWrapper();
        queryWrapper.eq("odr_customer",odrCustomer);
        return ordersMapper.delete(queryWrapper);
    }

    @Override
    public List<Orders> getContribute(String custName,Integer index,Integer showsize) {
        return  ordersMapper.getContribute(custName,index,showsize);
    }

    @Override
    public int getCount(String custName) {
        return ordersMapper.getCount(custName);
    }
}
