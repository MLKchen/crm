package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@ApiModel(value="com-crm-entity-CstActivity")
@Data
@TableName(value = "cst_activity")
public class CstActivity {
    /**
     * 编号
     */
    @TableId(value = "atv_id", type = IdType.INPUT)
    @ApiModelProperty(value="编号")
    private Long atvId;

    /**
     * 客户编号
     */
    @TableField(value = "atv_cust_no")
    @ApiModelProperty(value="客户编号")
    private String atvCustNo;

    /**
     * 客户姓名
     */
    @TableField(value = "atv_cust_name")
    @ApiModelProperty(value="客户姓名")
    private String atvCustName;

    /**
     * 时间
     */
    @TableField(value = "atv_date")
    @ApiModelProperty(value="时间")
    private String atvDate;

    /**
     * 地点
     */
    @TableField(value = "atv_place")
    @ApiModelProperty(value="地点")
    private String atvPlace;

    /**
     * 概要
     */
    @TableField(value = "atv_title")
    @ApiModelProperty(value="概要")
    private String atvTitle;

    /**
     * 详细信息
     */
    @TableField(value = "atv_desc")
    @ApiModelProperty(value="详细信息")
    private String atvDesc;

    /**
     * 备注
     */
    @TableField(value = "atv_remark")
    @ApiModelProperty(value="备注")
    private String atvRemark;
}