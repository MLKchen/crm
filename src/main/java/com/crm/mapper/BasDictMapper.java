package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.BasDict;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BasDictMapper extends BaseMapper<BasDict> {
}