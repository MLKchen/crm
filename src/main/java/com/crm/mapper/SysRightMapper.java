package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.SysRight;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRightMapper extends BaseMapper<SysRight> {
    /**
     * 根据角色编号查询权限集合
     * @param roleId
     * @return
     */
    List<SysRight> list(@Param("roleId") Long roleId);
}