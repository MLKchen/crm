package com.crm.service;

import com.crm.entity.BasDict;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface BasDictService extends IService<BasDict>{

    //根据条件(dict_type)获取数据
    public List<BasDict> getDataByType(String type);

    //根据地区名称查询地区编号
    public BasDict getRegionIid(String RegionName);
}
