package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-crm-entity-CstLinkman")
@Data
@TableName(value = "cst_linkman")
public class CstLinkman {
    /**
     * 编号
     */
    @TableId(value = "lkm_id",type = IdType.AUTO)
    @ApiModelProperty(value="编号")
    private Long lkmId;

    /**
     * 客户编号
     */
    @TableField(value = "lkm_cust_no")
    @ApiModelProperty(value="客户编号")
    private String lkmCustNo;

    /**
     * 客户姓名
     */
    @TableField(value = "lkm_cust_name")
    @ApiModelProperty(value="客户姓名")
    private String lkmCustName;

    /**
     * 联系人姓名
     */
    @TableField(value = "lkm_name")
    @ApiModelProperty(value="联系人姓名")
    private String lkmName;

    /**
     * 性别
     */
    @TableField(value = "lkm_sex")
    @ApiModelProperty(value="性别")
    private String lkmSex;

    /**
     * 职位
     */
    @TableField(value = "lkm_postion")
    @ApiModelProperty(value="职位")
    private String lkmPostion;

    /**
     * 办公电话
     */
    @TableField(value = "lkm_tel")
    @ApiModelProperty(value="办公电话")
    private String lkmTel;

    /**
     * 手机
     */
    @TableField(value = "lkm_mobile")
    @ApiModelProperty(value="手机")
    private String lkmMobile;

    /**
     * 备注
     */
    @TableField(value = "lkm_memo")
    @ApiModelProperty(value="备注")
    private String lkmMemo;
}