package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.CstCustomer;
import com.crm.entity.Orders;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class CstCustomerServiceTest {

    @Autowired
    private BasDictService basDictService;//数据字典
    @Autowired
    private CstCustomerService customerService;//客户信息操作
    @Autowired
    private CstLinkmanService linkmanService;//联系人操作
    @Autowired
    private CstActivityService activityService;//交往记录
    @Autowired
    private OrdersService ordersService;//历史订单
    @Autowired
    private OrdersLineService ordersLineService;//历史订单2详情
    @Autowired
    private CstLostService cstLostService;//留失管理部分

    @Test
    void getAll() {
        List<CstCustomer> CustomerList = null;
        IPage<CstCustomer> customerIPage = customerService.getAll("","","",
                "","",1);
        CustomerList = customerIPage.getRecords();
//        for (CstCustomer cstCustomer:CustomerList){
//            System.out.println(cstCustomer.getCustName());
//        }
        System.out.println(customerIPage.getPages());
    }

    @Test
    void getone(){
        //1.根据客户编号查询订单集合-获取订单详情的id
        List<Orders> ordersList = ordersService.getList("KH07122406540273");
        //2.根据ordersList集合中的id删除orders_line表中的数据
        for (Orders orders : ordersList){
            ordersLineService.del(String.valueOf(orders.getOdrId()));
        }
        //3.根据客户编号删除orders中的数据
        ordersService.del("KH07122406540273");
        //3.删除联系人
        linkmanService.delall("KH07122406540273");
        //4.删除交往记录
        activityService.delall("KH07122406540273");
        //5.删除
        customerService.delCustomer("KH07122406540273");
    }
}