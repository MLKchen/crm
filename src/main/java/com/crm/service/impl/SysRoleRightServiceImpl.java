package com.crm.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.mapper.SysRoleRightMapper;
import com.crm.entity.SysRoleRight;
import com.crm.service.SysRoleRightService;
@Service
public class SysRoleRightServiceImpl extends ServiceImpl<SysRoleRightMapper, SysRoleRight> implements SysRoleRightService{

}
