package com.crm.service.impl;

import com.crm.service.StorageService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.crm.mapper.StorageMapper;
import com.crm.entity.Storage;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StorageServiceImpl extends ServiceImpl<StorageMapper, Storage> implements StorageService {

    @Resource
    private StorageMapper storageMapper;

    @Override
    public List<Storage> listPage(Integer pageIndex,Integer pageSize,String prodName, String stkWarehouse) {
        return storageMapper.listPage(pageIndex,pageSize,prodName,stkWarehouse);
    }

    @Override
    public int listPageCount(String prodName, String stkWarehouse) {
        return storageMapper.listPageCount(prodName,stkWarehouse);
    }
}
