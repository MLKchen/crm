package com.crm.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PageUtil<T> implements Serializable {
    private Integer nowPage =1;//当前页数==》默认第一页
    private Integer showSize=2;//每页显示数据数
    private static Integer countPage;//符合条件的数据总数

    private Integer offset;//偏移量
    private Integer totalPageCount;//总页数

    //用户存储分页中显示的数据
    private List<T> list=new ArrayList<T>();

    public Integer getNowPage() {
        return nowPage;
    }

    public void setNowPage(Integer nowPage) {
        this.nowPage = nowPage;
    }

    public Integer getShowSize() {
        return showSize;
    }

    public void setShowSize(int showSize) {
        this.showSize = showSize;
    }

    public Integer getCountPage() {
        return countPage;
    }

    public void setCountPage(Integer countPage) {
        this.countPage = countPage;
    }

    //获取当前偏移量
    public Integer getOffset() {
        return (nowPage -1)*showSize;
    }

    public Integer getTotalPageCount() {
        return countPage%showSize==0?countPage/showSize:countPage/showSize+1;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }


}
