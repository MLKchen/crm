package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.CstActivity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CstActivityMapper extends BaseMapper<CstActivity> {
}