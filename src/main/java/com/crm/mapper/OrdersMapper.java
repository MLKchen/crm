package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {

    //获取客户贡献值
    public List<Orders> getContribute(@Param("custName") String custName,@Param("index")Integer index,
                                      @Param("showsize")Integer showsize);
    //获取客户贡献值的总条数
    public int getCount(@Param("custName") String custName);

}