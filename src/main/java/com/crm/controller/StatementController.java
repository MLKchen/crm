package com.crm.controller;

import com.alibaba.fastjson.JSON;
import com.crm.entity.CstCustomer;
import com.crm.entity.CstLost;
import com.crm.entity.CstService;
import com.crm.entity.Orders;
import com.crm.service.CstCustomerService;
import com.crm.service.CstLostService;
import com.crm.service.CstServiceService;
import com.crm.service.OrdersService;
import com.crm.utils.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.core.metadata.IPage;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 27/11/2022 上午9:14
 * Description: 统计报表模块
 */
@Controller
//@RequestMapping("/contrRpt")
public class StatementController {

    @Resource
    private OrdersService ordersService;//订单信息操作

    @Autowired
    private CstCustomerService customerService;//客户信息操作

    @Autowired
    private CstServiceService cstServiceService;

    @Autowired
    private CstLostService cstLostService;//留失管理部分


    @RequestMapping("/qwe")
    public String tott(){
        return "/echartsTEST";
    }

    //报表客户贡献
    @ResponseBody
    @RequestMapping("/contribute")
    public String tocontribute(){
        List<Orders> ordersList = ordersService.getContribute("",0,50);
        for (Orders orders:ordersList){
            orders.setValue(orders.getSumprice());
            orders.setName(orders.getCustName());
        }
        return JSON.toJSONString(ordersList);
    }

    //进入客户分析页面
    @RequestMapping("/contrRpt/list")
    public String toContribute(Model model,@RequestParam(defaultValue = "")String custName, @RequestParam(defaultValue = "1")Integer pageIndex){
        //总数
        int totalCount = ordersService.getCount(custName);
        int pageSize = 2;
        //分页操作
        PageUtil pages = new PageUtil();
        pages.setNowPage(pageIndex);
        pages.setShowSize(pageSize);
        pages.setCountPage(totalCount);
        int totalPageCount = pages.getTotalPageCount();//总页数
        List<Orders> ordersList = ordersService.getContribute(custName,pages.getOffset(),pages.getShowSize());

        //获取全部数据(不分页，用于生成Excel或者pdf文件)
        List<Orders> ordersListss = ordersService.getContribute(custName,0,totalCount+1);
        model.addAttribute("ordersListss",ordersListss);//不分页数据

        //分页数据返回
        model.addAttribute("showSize",totalPageCount);//总页数
        model.addAttribute("index",pages.getNowPage());//当前页
        model.addAttribute("total",totalCount);//记录数

        //数据列表
        model.addAttribute("ordersList",ordersList);//分页数据

        //查询条件返回
        model.addAttribute("custName",custName);//记录数
        return "/statement/contribute";
    }


    //报表客户构成
    @ResponseBody
    @RequestMapping("/constitute")
    public String toconstitute(){
        List<CstCustomer> cstCustomerlist = customerService.getCount();
        return JSON.toJSONString(cstCustomerlist);
    }

    //进入客户构成页面
    @RequestMapping("/consRpt/list")
    public String toConstitute(Model model,@RequestParam(defaultValue = "1")Integer pageIndex){
        List<CstCustomer> cstCustomerlist = customerService.getCount();
        model.addAttribute("cstCustomer",cstCustomerlist);
        return "/statement/constitute";
    }


    //报表客户服务
    @ResponseBody
    @RequestMapping("/service")
    public String toservice(){
        List<CstService> cstServiceList = cstServiceService.getCountservice();
        return JSON.toJSONString(cstServiceList);
    }

    //进入客户服务列表
    @RequestMapping("/svrRpt/list")
    public String toService(Model model){
        List<CstService> cstServiceList = cstServiceService.getCountservice();
        model.addAttribute("cstServiceList",cstServiceList);
        return "/statement/service";
    }


    //进入流失列表
    @RequestMapping("/lostRpt/list")
    public String toLoss(Model model, @RequestParam(defaultValue = "")String lstCustName,
                         @RequestParam(defaultValue = "")String lstCustManagerName,
                         @RequestParam(defaultValue = "1")Integer pageIndex){
        List<CstLost> cstLostList = null;
        IPage<CstLost> cstLostIPage = cstLostService.getlostList(lstCustName,lstCustManagerName,pageIndex);
        cstLostList = cstLostIPage.getRecords();

        model.addAttribute("cstLostList",cstLostList);

        //查询条件返回
        model.addAttribute("lstCustName",lstCustName);
        model.addAttribute("lstCustManagerName",lstCustManagerName);

        //分页数据传递
        model.addAttribute("total",cstLostIPage.getTotal());//总记录数
        model.addAttribute("index",cstLostIPage.getCurrent());//当前页数
        model.addAttribute("showSize",cstLostIPage.getPages());//总页数

        return "/statement/lostlist";
    }
}
