package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-crm-entity-Orders")
@Data
@TableName(value = "orders")
public class Orders {
    /**
     * 订单编号
     */
    @TableId(value = "odr_id", type = IdType.AUTO)
    @ApiModelProperty(value="订单编号")
    private Long odrId;

    /**
     * 客户编号
     */
    @TableField(value = "odr_customer")
    @ApiModelProperty(value="客户编号")
    private String odrCustomer;

    /**
     * 日期
     */
    @TableField(value = "odr_date")
    @ApiModelProperty(value="日期")
    private Date odrDate;

    /**
     * 送货地址
     */
    @TableField(value = "odr_addr")
    @ApiModelProperty(value="送货地址")
    private String odrAddr;

    /**
     * 状态
     */
    @TableField(value = "odr_status")
    @ApiModelProperty(value="状态")
    private String odrStatus;

    //客户名称
    @TableField(exist = false)
    private String custName;

    //总金额
    @TableField(exist = false)
    private String sumprice;

    @TableField(exist = false)
    private String value;

    @TableField(exist = false)
    private String name;
}