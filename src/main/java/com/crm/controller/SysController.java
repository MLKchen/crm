package com.crm.controller;

import com.crm.entity.SysUser;
import com.crm.service.SysUserService;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;

/**
 * @author CCB
 * date: 22/11/2022 下午2:00
 * Description: 描述
 */
@Controller
public class SysController {

    private Logger logger = Logger.getLogger(SysController.class);

    //Redis使用，String类型
    @Resource
    private StringRedisTemplate stringRedis;

    @Resource
    public SysUserService userService;

    @RequestMapping(value = "/login")
    public String toLogin() {
        return "login";
    }

    //验证登录用户
    @PostMapping(value = "/checkLogin")
    public String Login(Model model, HttpSession session, String usrName, String usrPassword) {
        SysUser user = null;
        try {
            UsernamePasswordToken token = new UsernamePasswordToken(usrName,usrPassword);
            Subject subject = SecurityUtils.getSubject();
            subject.login(token);//认证，登录
            user = (SysUser) subject.getPrincipal();
        }catch (UnknownAccountException | IncorrectCredentialsException e){
            //账号密码错误
        }catch (LockedAccountException e){
            //用户的状态不为1
        }catch (AuthenticationException e){
            //其它异常
        }

        if (stringRedis.hasKey(usrName)){
            if(stringRedis.opsForValue().get(usrName).equals("3")){
                model.addAttribute("message", "您的账户已锁定请" + stringRedis.getExpire(usrName, TimeUnit.MINUTES) + "分钟后再试！");
                return "forward:/login";
            }
        }
        if (user != null) {
            logger.info("登录成功！");
            session.setAttribute("User", user);
            stringRedis.delete(usrName);
            return "main";
        } else {
            logger.info("登录失败！");
            if (stringRedis.hasKey(usrName)) {
                stringRedis.boundValueOps(usrName).increment(1);
                if (stringRedis.opsForValue().get(usrName).equals("3")) {
                    model.addAttribute("message", "您的账户已锁定请5分钟后再试！");
                } else {
                    model.addAttribute("message", "用户名或密码错误！您还有" + (3 - Integer.valueOf(stringRedis.opsForValue().get(usrName))) + "次机会");
                }
                stringRedis.expire(usrName, 5, TimeUnit.MINUTES);
            } else {
                stringRedis.opsForValue().set(usrName, "1", 5, TimeUnit.MINUTES);
                model.addAttribute("message", "用户名或密码错误！您还有2次机会");
            }
            return "forward:/login";
        }
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        session.setAttribute("User", null);
        return "redirect:/login";
    }

    @RequestMapping("/403")
    public String error(){
        return "/403";
    }

}
