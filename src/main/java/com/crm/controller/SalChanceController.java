package com.crm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.crm.entity.SalChance;
import com.crm.entity.SalPlan;
import com.crm.entity.SysUser;
import com.crm.service.SalChanceService;
import com.crm.service.SalPlanService;
import com.crm.service.SysUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * @author CLH
 * date: 2022/11/21 10:28
 * Description: 销售机会控制层
 */
@Controller
@RequestMapping("/chance")
public class SalChanceController {

    @Resource
    private SysUserService sysUserService;
    @Resource
    private SalChanceService salChanceService;
    @Resource
    private SalPlanService salPlanService;

    /**
     * 进入资源列表页面
     *
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model,
                       @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                       @RequestParam(required = false) String custName,
                       @RequestParam(required = false) String chcDesc) {
        Page page = new Page(pageIndex, 5);//页码
        /*查询条件*/
        QueryWrapper<SalChance> wrapper = new QueryWrapper<SalChance>();
        if (custName != null && !custName.equals("")) {//判断名称不为空
            wrapper.eq("chc_cust_name", custName);
            model.addAttribute("custName", custName);
        }
        if (chcDesc != null && !chcDesc.equals("")) {//判断概要不为空
            wrapper.eq("chc_desc", chcDesc);
            model.addAttribute("chcDesc", chcDesc);
        }
        IPage chancePage = salChanceService.page(page, wrapper);
        System.out.println("页码：" + chancePage.getCurrent());
        System.out.println("总页数：" + chancePage.getPages());
        System.out.println("总行数：" + chancePage.getTotal());
        System.out.println("集合：" + chancePage.getRecords().toString());
        model.addAttribute("chancePage", chancePage);
        return "/chance/list";
    }

    /**
     * 进入新增页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model) {
        List<SysUser> sysUserList = sysUserService.list();
        if (sysUserList.size() != 0) {
            model.addAttribute("users", sysUserList);
        }
        return "/chance/add";
    }

    /**
     * 新增销售数据
     *
     * @param salChance
     * @return
     */
    @PostMapping("/save")
    public String save(SalChance salChance, HttpSession session) {
        salChance.setChcCreateId(((SysUser)session.getAttribute("User")).getUsrId());//当前登录用户编号
        salChance.setChcCreateBy(((SysUser)session.getAttribute("User")).getUsrName());//当前登录用户名称
        if (salChance.getChcDueId()==null || salChance.getChcDueId()==0){//判断没有选择指派人
            salChance.setChcDueId(null);
            salChance.setChcStatus("1");//状态改未分配
        }else {
            SysUser sysUser = sysUserService.getById(salChance.getChcDueId());//根据指派人编号查询指派人信息
            salChance.setChcDueTo(sysUser.getUsrName());//添加指派人名称
            salChance.setChcStatus("2");//状态改为指派中
        }
        if (salChanceService.save(salChance)) {
            return "redirect:/chance/list";
        } else {
            return "redirect:/chance/add";
        }
    }

    /**
     * 进入修改页面
     *
     * @param model
     * @param chcId
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Model model, Long chcId) {
        SalChance chance = salChanceService.getById(chcId);
        if (chance.getChcDueDate() != null){
            chance.setChcDueDate(chance.getChcDueDate().split(" ")[0]);
        }
        if (chance != null) {
            model.addAttribute("chance", chance);
        }
        List<SysUser> sysUserList = sysUserService.list();
        if (sysUserList.size() != 0) {
            model.addAttribute("users", sysUserList);
        }
        return "/chance/edit";
    }

    /**
     * 修改销售数据
     *
     * @param salChance
     * @return
     */
    @PostMapping("/update")
    public String update(SalChance salChance) {
        /*查询条件*/
        QueryWrapper<SalChance> wrapper = new QueryWrapper<SalChance>();
        if (salChance.getChcDueId()==null || salChance.getChcDueId()==0){//判断没有选择指派人
            salChance.setChcDueId(null);
            salChance.setChcStatus("1");//状态改未分配
        }else {
            SysUser sysUser = sysUserService.getById(salChance.getChcDueId());//根据指派人编号查询指派人信息
            salChance.setChcDueTo(sysUser.getUsrName());//添加指派人名称
            salChance.setChcStatus("2");//状态改为指派中
        }
        wrapper.eq("chc_id", salChance.getChcId());
        if (salChanceService.update(salChance, wrapper)) {
            return "redirect:/chance/list";
        } else {
            return "redirect:/chance/edit";
        }
    }

    /**
     * Ajax删除销售数据
     *
     * @param chcId
     * @return
     */
    @ResponseBody
    @GetMapping("/del/{chcId}")
    public Object del(@PathVariable("chcId") Long chcId,HttpSession session) {
        /*条件管理器*/
        QueryWrapper<SalChance> salChanceQueryWrapper = new QueryWrapper<SalChance>();
        salChanceQueryWrapper.eq("chc_id", chcId);

        /*Map返回结果*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        SalChance salChance = salChanceService.getById(chcId);
        if (!salChance.getChcStatus().equals("1")) {
            hashMap.put("delResult", "false");//该销售机会已在指派中
        } else {
            if (salChance.getChcCreateBy().equals((((SysUser)session.getAttribute("User")).getUsrName()))) {//判断该销售机会创建人是登录用户
                if (salChanceService.remove(salChanceQueryWrapper)) {
                    hashMap.put("delResult", "true");//删除成功
                } else {
                    hashMap.put("delResult", "notexist");//删除失败
                }
            }else {
                hashMap.put("delResult", "jurisdiction");//创建人不是登录用户
            }
        }
        return JSON.toJSONString(hashMap);
    }
}
