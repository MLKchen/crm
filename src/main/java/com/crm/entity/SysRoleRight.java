package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-crm-entity-SysRoleRight")
@Data
@TableName(value = "sys_role_right")
public class SysRoleRight {
    @TableId(value = "rf_id", type = IdType.AUTO)
    @ApiModelProperty(value="")
    private Long rfId;

    @TableField(value = "rf_role_id")
    @ApiModelProperty(value="")
    private Long rfRoleId;

    @TableField(value = "rf_right_code")
    @ApiModelProperty(value="")
    private String rfRightCode;
}