package com.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel(value="com-crm-entity-SysRole")
@Data
@TableName(value = "sys_role")
public class SysRole {
    /**
     * 编号
     */
    @TableId(value = "role_id", type = IdType.AUTO)
    @ApiModelProperty(value="编号")
    private Long roleId;

    /**
     * 角色名称
     */
    @TableField(value = "role_name")
    @ApiModelProperty(value="角色名称")
    private String roleName;

    /**
     * 角色描述
     */
    @TableField(value = "role_desc")
    @ApiModelProperty(value="角色描述")
    private String roleDesc;

    /**
     * 状态
     */
    @TableField(value = "role_flag")
    @ApiModelProperty(value="状态")
    private Integer roleFlag;

    @TableField(exist = false)
    private List<SysRight> sysRightList;
}