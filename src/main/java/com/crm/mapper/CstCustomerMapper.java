package com.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.crm.entity.CstCustomer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CstCustomerMapper extends BaseMapper<CstCustomer> {
    //客户构成列表=》用于报表
    List<CstCustomer> getCount();
}