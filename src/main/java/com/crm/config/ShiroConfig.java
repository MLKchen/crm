package com.crm.config;

import com.crm.entity.SysRight;
import com.crm.service.SysRightService;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.shiro.mgt.SecurityManager;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CCB
 * date: 12/12/2022 下午2:13
 * Description: Shiro
 */
@Configuration
public class ShiroConfig {

    @Resource
    private SysRightService rightService;

    @Bean
    public MyShiroRealm myShiroRealm(){//自定义Realm
        MyShiroRealm shiroRealm = new MyShiroRealm();
        return shiroRealm;
    }

    @Bean
    public SecurityManager securityManager(){//安全管理器
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        //注入Realm
        securityManager.setRealm(myShiroRealm());
        return securityManager;
    }

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager){//Shiro权限过滤器：权限验证

        ShiroFilterFactoryBean shiroFilterFactory = new ShiroFilterFactoryBean();

        //注入securityManager
        shiroFilterFactory.setSecurityManager(securityManager);
        //权限验证：使用Filter控制资源(URL)的访问
        shiroFilterFactory.setLoginUrl("/login");//登录
//        shiroFilterFactory.setSuccessUrl("/main");
        shiroFilterFactory.setUnauthorizedUrl("/403");//错误跳转页面

        Map<String,String> filterChainDefinitionMap = new LinkedHashMap<String,String>();//必须有序集合
        filterChainDefinitionMap.put("/css/**","anon");
        filterChainDefinitionMap.put("/fonts/**","anon");
        filterChainDefinitionMap.put("/images/**","anon");
        filterChainDefinitionMap.put("/js/**","anon");
        filterChainDefinitionMap.put("/localcss/**","anon");
        filterChainDefinitionMap.put("/localjs/**","anon");

        filterChainDefinitionMap.put("/checkLogin","anon");
        filterChainDefinitionMap.put("/logout","logout");

        List<SysRight> rights = rightService.list();
        for (SysRight right : rights){
            if(right.getRightUrl()!=null && !right.getRightUrl().trim().equals("")){
                filterChainDefinitionMap.put(right.getRightUrl(),"perms["+right.getRightCode()+"]");
            }
        }

        //配置认证访问
        filterChainDefinitionMap.put("/**","authc");

        shiroFilterFactory.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactory;
    }

}
