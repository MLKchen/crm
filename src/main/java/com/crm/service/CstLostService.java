package com.crm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.crm.entity.CstActivity;
import com.crm.entity.CstLost;
import com.baomidou.mybatisplus.extension.service.IService;
public interface CstLostService extends IService<CstLost>{

    public IPage<CstLost> lostList(String lstCustName, String lstCustManagerName,String lstStatus, Integer index);

    public CstLost getLostBylstId(String lstId);

    public int temporarymodify(CstLost cstLost);

    public int losemodify(CstLost cstLost);

    public IPage<CstLost> getlostList(String lstCustName, String lstCustManagerName, Integer index);

}
